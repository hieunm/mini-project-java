package com.devcamp.pizza365crudrestapi.controllers;

import com.devcamp.pizza365crudrestapi.errors.ResourceNotFoundException;
import com.devcamp.pizza365crudrestapi.entities.Country;
import com.devcamp.pizza365crudrestapi.services.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class CountryController {
	@Autowired
	private CountryService countryService;

	@CrossOrigin
	@PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@RequestBody Country cCountry) {
		try {
			Country country = countryService.createCountry(cCountry);
			if (country != null) {
				return new ResponseEntity<>(country, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PutMapping("/countries/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody Country cCountry) {
		try {
			Country country = countryService.updateCountry(id, cCountry);
			if (country != null) {
				return new ResponseEntity<>(country, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@DeleteMapping("/countries/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryService.deleteCountryById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (ResourceNotFoundException e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/{id}")
	public ResponseEntity<Country> getCountryById(@PathVariable Long id) {
		try {
			Country country = countryService.getCountryById(id);
			if (country != null) {
				return new ResponseEntity<>(country, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries")
	public ResponseEntity<List<Country>> getAllCountry() {
		try {
			return new ResponseEntity<List<Country>>(countryService.getAllCountry(), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/count")
	public ResponseEntity<Long> countCountry() {
		try {
			return new ResponseEntity<>(countryService.countCountry(), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/check/{id}")
	public ResponseEntity<Boolean> checkCountryById(@PathVariable Long id) {
		try {
			return new ResponseEntity<>(countryService.checkCountryById(id), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/pagination")
	public ResponseEntity<List<Country>> getCountryPagination(
			@RequestParam(value = "page", defaultValue = "1") String page,
			@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			List<Country> countryPage = countryService.getCountryPagination(page, size);
			return new ResponseEntity<>(countryPage, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
