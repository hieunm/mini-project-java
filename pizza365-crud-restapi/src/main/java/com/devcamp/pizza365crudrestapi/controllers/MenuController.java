package com.devcamp.pizza365crudrestapi.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365crudrestapi.entities.Menu;
import com.devcamp.pizza365crudrestapi.errors.ResourceNotFoundException;
import com.devcamp.pizza365crudrestapi.services.MenuService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class MenuController {
    @Autowired
    private MenuService menuService;

    @GetMapping("/menu")
    public ResponseEntity<List<Menu>> getAllMenu() {
        try {
            List<Menu> menu = menuService.getAllMenu();
            if (menu != null && !menu.isEmpty()) {
                return new ResponseEntity<List<Menu>>(menu, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy menu theo {id}
    @GetMapping("/menu/{menuId}")
    public ResponseEntity<Object> getMenuById(@PathVariable("menuId") Long id) {
        try {
            Menu menu = menuService.GetMenuById(id);
            return ResponseEntity.ok().body(menu);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/menu")
    public ResponseEntity<Object> createMenu(@Valid @RequestBody Menu pMenu) {
        try {
            Menu menu = menuService.createMenu(pMenu);
            return new ResponseEntity<>(menu, HttpStatus.CREATED);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("menu/{menuId}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "menuId") Long id, @Valid @RequestBody Menu pMenu) {
        try {
            Menu menu = menuService.updateMenu(id, pMenu);
            return new ResponseEntity<>(menu, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("menu/{menuId}")
    public ResponseEntity<Object> deleteUser(@PathVariable(name = "menuId") Long id) {
        try {
            menuService.deleteMenu(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
