package com.devcamp.pizza365crudrestapi.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365crudrestapi.entities.Drink;
import com.devcamp.pizza365crudrestapi.services.DrinkService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DrinkController {
    @Autowired
    private DrinkService drinkService;

    //lấy toàn bộ drinks
    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrinks() {
        try {
            List<Drink> drinks = drinkService.getAllDrinks();//lấy danh sách drinks
            if (drinks.size() == 0) {
                return new ResponseEntity<List<Drink>>(HttpStatus.NOT_FOUND);//phản hồi 404
            } else {
                return new ResponseEntity<List<Drink>>(drinks, HttpStatus.OK);//phản hồi ok
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Lấy drink theo {id}
	@GetMapping("/drinks/{id}") // Dùng phương thức GET
	public ResponseEntity<Drink> getDrinkById(@PathVariable("id") long id) {
		try {
			Drink drink = drinkService.findDrinkById(id);
			if (drink != null) {
				return ResponseEntity.ok().body(drink);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.internalServerError().build();
		}
	}
    
    // Tạo MỚI drink
	@PostMapping("/drinks") // Dùng phương thức POST
    //@Valid sẽ check input dựa theo validation trong model được stringcontainer quản lý trước khi bắt đầu khối code controller
    //ko hợp lệ sẽ ném lỗi vào globalexception
	public ResponseEntity<Object> createDrink(@Valid @RequestBody Drink pDrink) {
		try {
			Drink drinkFound = drinkService.findDrinkById(pDrink.getId());
			if (drinkFound != null) {
				return ResponseEntity.unprocessableEntity().body("Drink already exsit");
			}

			Drink drink = drinkService.createDrink(pDrink);
			return new ResponseEntity<>(drink, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
		}
	}

    // Sửa/update drink theo id
	@PutMapping("/drinks/{id}") // Dùng phương thức PUT
	public ResponseEntity<Object> updateDrinkById(@PathVariable("id") long id, @Valid @RequestBody Drink pDrink) {
		try {
			Drink drink = drinkService.updateDrinkById(id, pDrink);
			if (drink == null) {
				return ResponseEntity.badRequest().body("Failed to get specified Drink: " + id + "  for update.");
			} else {
				return new ResponseEntity<>(drink, HttpStatus.OK);
			}
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Update specified Drink:" + e.getCause().getCause().getMessage());
		}
	}

    // Xoá/delete drink theo {id}
	@DeleteMapping("/drinks/{id}") // Dùng phương thức DELETE
	public ResponseEntity<Drink> deleteDrinkById(@PathVariable("id") long id) {
		try {
			Drink drink = drinkService.findDrinkById(id);
			if (drink != null) {
				drinkService.deleteDrinkById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	// Xoá/delete tất cả drink
	@DeleteMapping("/drinks") // Dùng phương thức DELETE
	public ResponseEntity<Drink> deleteAllDrink() {
		try {
			drinkService.deleteAllDrink();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
