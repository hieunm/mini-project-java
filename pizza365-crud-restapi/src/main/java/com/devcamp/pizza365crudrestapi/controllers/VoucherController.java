package com.devcamp.pizza365crudrestapi.controllers;

import java.util.*;

import com.devcamp.pizza365crudrestapi.entities.Voucher;
import com.devcamp.pizza365crudrestapi.repositories.VoucherRepository;
import com.devcamp.pizza365crudrestapi.services.VoucherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class VoucherController {
	@Autowired
	VoucherRepository pVoucherRepository;
	@Autowired
	VoucherService pVoucherService;

	// Lấy danh sách voucher dùng service.
	@GetMapping("/vouchers") // Dùng phương thức GET
	public ResponseEntity<List<Voucher>> getAllVouchersBySevice() {
		try {
			return new ResponseEntity<>(pVoucherService.getVoucherList(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// Lấy voucher theo {id} dùng service
	@GetMapping("/vouchers/{id}") // Dùng phương thức GET
	public ResponseEntity<Voucher> getCVoucherById(@PathVariable("id") long id) {
		try {
			Voucher voucher = pVoucherService.findVoucherById(id);
			if (voucher != null) {
				return ResponseEntity.ok().body(voucher);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.internalServerError().build();
		}
	}

	// Tạo MỚI voucher dùng service sử dụng phương thức POST
	@PostMapping("/vouchers") // Dùng phương thức POST
	public ResponseEntity<Object> createCVoucher(@RequestBody Voucher pVouchers) {
		try {
			Voucher voucher = pVoucherService.findVoucherById(pVouchers.getId());
			if (voucher != null) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
			}

			Voucher _vouchers = pVoucherService.createCVoucher(pVouchers);
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	// Sửa/update voucher theo {id} dùng service, sử dụng phương thức PUT
	@PutMapping("/vouchers/{id}") // Dùng phương thức PUT
	public ResponseEntity<Object> updateCVoucherById(@PathVariable("id") long id, @RequestBody Voucher pVouchers) {
		try {
			Voucher voucher = pVoucherService.updateCVoucherById(id, pVouchers);
			if (voucher == null) {
				return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
			} else {
				return new ResponseEntity<>(voucher, HttpStatus.OK);
			}
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
		}
	}

	// Xoá/delete voucher theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/vouchers/{id}") // Dùng phương thức DELETE
	public ResponseEntity<Voucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			Voucher voucher = pVoucherService.findVoucherById(id);
			if (voucher != null) {
				pVoucherService.deleteCVoucherById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	// Xoá/delete tất cả voucher dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/vouchers") // Dùng phương thức DELETE
	public ResponseEntity<Voucher> deleteAllCVoucher() {
		try {
			pVoucherService.deleteAllCVoucher();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
