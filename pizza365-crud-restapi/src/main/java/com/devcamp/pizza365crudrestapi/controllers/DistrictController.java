package com.devcamp.pizza365crudrestapi.controllers;

import com.devcamp.pizza365crudrestapi.entities.District;
import com.devcamp.pizza365crudrestapi.services.DistrictService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class DistrictController {
	@Autowired
	private DistrictService districtService;

	@CrossOrigin
	@PostMapping("/districts")
	public ResponseEntity<Object> createDistrict(@RequestBody District cDistrict) {
		try {
			District district = districtService.createDistrict(cDistrict);
			if (district != null) {
				return new ResponseEntity<>(district, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/districts/{id}")
	public ResponseEntity<Object> updateDistrict(@PathVariable("id") Long id, @RequestBody District cDistrict) {
		try {
			District district = districtService.updateDistrict(id, cDistrict);
			if (district != null) {
				return new ResponseEntity<>(district, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.err.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@DeleteMapping("/districts/{id}")
	public ResponseEntity<Object> deleteDistrictById(@PathVariable Long id) {
		try {
			districtService.deleteDistrictById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/districts/{id}")
	public ResponseEntity<Object> getRegionById(@PathVariable Long id) {
		try {
			District district = districtService.getDistrictById(id);
		if (district != null) {
			return new ResponseEntity<>(district, HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/districts")
	public ResponseEntity<List<District>> getAllDistrict() {
		try {
			return new ResponseEntity<>(districtService.getAllDistrict(), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
