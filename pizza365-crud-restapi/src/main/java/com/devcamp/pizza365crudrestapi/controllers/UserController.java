package com.devcamp.pizza365crudrestapi.controllers;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365crudrestapi.entities.User;
import com.devcamp.pizza365crudrestapi.entities.Order;
import com.devcamp.pizza365crudrestapi.errors.ResourceNotFoundException;
import com.devcamp.pizza365crudrestapi.services.UserService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {
        try {
            List<User> users = userService.getAllUsers();
            return ResponseEntity.ok().body(users);
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/users/{userId}/orders")
    public ResponseEntity<Set<Order>> getOrdersByUserId(@PathVariable(name = "userId") Long id) {
        try {
            Set<Order> orders = userService.getOrdersByUserId(id);
            if (orders == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<Set<Order>>(orders, HttpStatus.OK);
            }
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/users/{userId}/orders{orderId}")
    public ResponseEntity<Object> getInfoOrderOfUserById(@PathVariable(name = "userId") Long userId, @PathVariable(name = "orderId") Long orderId) {
        try {
            Order order = userService.getInfoOrderOfUserById(userId, orderId);
            return new ResponseEntity<>(order, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/users/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable(name = "userId") Long id) {
        try {
            User userFound = userService.getUserById(id);
            if (userFound != null) {
                return ResponseEntity.ok().body(userFound);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User pUser) {
        try {
            User user = userService.createUser(pUser);
            if (user == null) {
                return ResponseEntity.unprocessableEntity().body("User already exist");
            } else {
                return new ResponseEntity<Object>(user, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
 
    @PutMapping("users/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "userId") Long id, @Valid @RequestBody User pUser) {
        try {
            User user = userService.updateUser(id, pUser);
            if (user == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(user, HttpStatus.OK);
            }
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("users/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable(name = "userId") Long id) {
        try {
            userService.deleteUser(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getCause().getCause().getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@GetMapping("/users/count")
	public ResponseEntity<Long> countUser() {
		try {
			return new ResponseEntity<>(userService.countUser(), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/check/{id}")
	public ResponseEntity<Boolean> checkUserById(@PathVariable Long id) {
		try {
			return new ResponseEntity<>(userService.checkUserById(id), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/pagination")
	public ResponseEntity<List<User>> getUserPagination(
			@RequestParam(value = "page", defaultValue = "1") String page,
			@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			List<User> userPage = userService.getUserPagination(page, size);
			return new ResponseEntity<>(userPage, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
