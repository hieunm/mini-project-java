package com.devcamp.pizza365crudrestapi.controllers;

import com.devcamp.pizza365crudrestapi.entities.Region;
import com.devcamp.pizza365crudrestapi.services.RegionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionService regionService;

	@CrossOrigin
	@PostMapping("/regions")
	public ResponseEntity<Object> createRegion(@RequestBody Region cRegion) {
		try {
			Region region = regionService.createRegion(cRegion);
			if (region != null) {
				return new ResponseEntity<>(region, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/regions/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody Region cRegion) {
		try {
			Region region = regionService.updateRegion(id, cRegion);
			if (region != null) {
				return new ResponseEntity<>(region, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.err.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@DeleteMapping("/regions/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionService.deleteRegionById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/regions/{id}")
	public ResponseEntity<Region> getRegionById(@PathVariable Long id) {
		try {
			Region region = regionService.getRegionById(id);
		if (region != null) {
			return new ResponseEntity<>(region, HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/regions")
	public ResponseEntity<List<Region>> getAllRegion() {
		try {
			return new ResponseEntity<>(regionService.getAllRegion(), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions")
	public ResponseEntity<List<Region>> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		try {
			List<Region> regions = regionService.getRegionsByCountry(countryId);
			if (!regions.isEmpty()) {
				return new ResponseEntity<>(regions, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions/{id}")
	public ResponseEntity<Region> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		try {
			Optional<Region> regionsOptional = regionService.getRegionByRegionAndCountry(countryId, regionId);
			if (regionsOptional.isPresent()) {
				return new ResponseEntity<>(regionsOptional.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions/count")
	public ResponseEntity<Integer> countRegionByCountryId(@PathVariable(value = "countryId") Long countryId) {
		try {
			return new ResponseEntity<>(regionService.countRegionByCountryId(countryId), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/regions/check/{id}")
	public ResponseEntity<Boolean> checkRegionById(@PathVariable Long id) {
		try {
			return new ResponseEntity<>(regionService.checkRegionById(id), HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
