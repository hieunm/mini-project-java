package com.devcamp.pizza365crudrestapi.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Nhập order code")
    @Size(min = 2, max = 20, message = "Order code phải có độ dài từ 2 đến 20 kí tự")
    @Column(name = "order_code", unique = true)
    private String orderCode;

    @NotNull(message = "Nhập pizza size")
    @Size(min = 1, max = 5, message = "Pizza size phải có độ dài từ 1 đến 5 kí tự")
    @Column(name = "pizza_size")
    private String pizzaSize;

    @NotEmpty(message = "Nhập pizza type")
    @Size(min = 2, max = 20, message = "Pizza type phải có độ dài từ 2 đến 20 kí tự")
    @Column(name = "pizza_type")
    private String pizzaType;

    @Column(name = "voucher_code")
    private String voucherCode;

    @NotNull(message = "Nhập mức giá")
    @Range(min = 10000, max = 500000, message = "Mức giá phải nằm trong khoảng từ 10k đến 500k")
    @Column(name = "price")
    private long price;

    @NotNull(message = "Nhập số tiền đã thanh toán")
    @Range(min = 0, message = "Số tiền đã thanh toán phải lớn hơn 0")
    @Column(name = "paid")
    private long paid;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date updatedAt;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Transient
    private long userId;

    @Transient
    private String userFullName;

    public long getUserId() {
        return this.getUser().getId();
    }

    public void setUserId(long userId) {
        this.user.setId(userId);
    }

    public String getUserFullName() {
        return this.user.getFullName();
    }

    public void setUserFullName(String userFullName) {
        this.user.setFullName(userFullName);;
    }

    public Order() {
    }

    public Order(long id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price,
            long paid, Date createdAt, Date updatedAt) {
        this.id = id;
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPaid() {
        return paid;
    }

    public void setPaid(long paid) {
        this.paid = paid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
