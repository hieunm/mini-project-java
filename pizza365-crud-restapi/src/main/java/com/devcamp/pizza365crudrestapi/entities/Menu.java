package com.devcamp.pizza365crudrestapi.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "menu")
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull(message = "Nhập kích cỡ")
	@Column(name = "kick_co")
	private char kichCo;

	@NotEmpty(message = "Nhập đường kính")
	@Column(name = "duong_kinh")
	private String duongKinh;

	@NotNull(message = "Nhập số lượng sườn nướng")
	@Range(min = 1, message = "Sườn nướng phải >= 1")
	@Column(name = "suon_nuong")
	private int suonNuong;

	@NotEmpty(message = "Nhập salad")
	@Column(name = "salad")
	private String salad;

	@NotNull(message = "Nhập số lượng nước ngọt")
	@Range(min = 1, message = "Nước ngọt phải >= 1")
	@Column(name = "nuoc_ngot")
	private int nuocNgot;

	@NotNull(message = "Nhập số tiền")
	@Range(min = 1000, message = "Số tiền phải >= 1000")
	@Column(name = "thanh_tien")
	private int thanhTien;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", updatable = false)
	@CreatedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date updatedAt;

	public Menu() {
	}

	public Menu(long id, char kichCo, String duongKinh, int suonNuong, String salad, int nuocNgot, int thanhTien,
			Date createdAt, Date updatedAt) {
		this.id = id;
		this.kichCo = kichCo;
		this.duongKinh = duongKinh;
		this.suonNuong = suonNuong;
		this.salad = salad;
		this.nuocNgot = nuocNgot;
		this.thanhTien = thanhTien;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public char getKichCo() {
		return kichCo;
	}

	public void setKichCo(char kichCo) {
		this.kichCo = kichCo;
	}

	public String getDuongKinh() {
		return duongKinh;
	}

	public void setDuongKinh(String duongKinh) {
		this.duongKinh = duongKinh;
	}

	public int getSuonNuong() {
		return suonNuong;
	}

	public void setSuonNuong(int suonNuong) {
		this.suonNuong = suonNuong;
	}

	public String getSalad() {
		return salad;
	}

	public void setSalad(String salad) {
		this.salad = salad;
	}

	public int getThanhTien() {
		return thanhTien;
	}

	public void setThanhTien(int thanhTien) {
		this.thanhTien = thanhTien;
	}

	public int getNuocNgot() {
		return nuocNgot;
	}

	public void setNuocNgot(int nuocNgot) {
		this.nuocNgot = nuocNgot;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public String toString() {
		return this.kichCo + "{kichCo=" + this.kichCo + ", duongKinh=" + this.duongKinh + ", suonNuong="
				+ this.suonNuong + ", salad=" + this.salad
				+ ", nuocNgot=" + this.nuocNgot + ", thanhTien=" + this.thanhTien + "}";
	}
}
