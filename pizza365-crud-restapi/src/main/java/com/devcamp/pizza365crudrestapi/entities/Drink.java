package com.devcamp.pizza365crudrestapi.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity // Đánh dấu lớp này là một entity (đối tượng) của cơ sở dữ liệu
@Table(name = "drinks") // Chỉ định tên bảng trong cơ sở dữ liệu mà entity này tương ứng
public class Drink {
    @Id // Đánh dấu trường này là khóa chính (primary key) trong bảng
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Chỉ định cách sinh giá trị cho trường ID tự động AUTO(dùng sequence table)
    private long id;

    @NotNull(message = "Nhập mã nước uống")
    @Size(min = 2, max = 10, message = "Mã nước uống phải có độ dài trong khoảng từ 2 đến 10 ký tự")
    @Column(name = "ma_nuoc_uong", unique = true) // Chỉ định tên cột trong bảng tương ứng với trường này
    private String maNuocUong;

    @NotEmpty(message = "Nhập tên nước uống")
    @Size(min = 2, max = 20, message = "Tên nước uống phải có độ dài trong khoảng từ 2 đến 20 ký tự")
    @Column(name = "ten_nuoc_uong") // Chỉ định tên cột trong bảng tương ứng với trường này
    private String tenNuocUong;

    @NotNull(message = "Nhập giá nước uống")
    @Range(min = 0, max = 500000, message = "Giá nước uống phải lớn hơn hoặc bằng 0 và nhỏ hơn 500k")
    @Column(name = "gia_nuoc_uong") // Chỉ định tên cột trong bảng tương ứng với trường này
    private long price;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao", nullable = true, updatable = false) // Chỉ định tên cột trong bảng tương ứng với trường này
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat", nullable = true) // Chỉ định tên cột trong bảng tương ứng với trường này
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayCapNhat;
    
    public Drink() {
    }

    public Drink(long id, String maNuocUong, String tenNuocUong, long price, Date ngayTao, Date ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public Date getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }
    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
}

