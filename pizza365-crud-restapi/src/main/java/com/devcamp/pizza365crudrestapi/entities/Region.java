package com.devcamp.pizza365crudrestapi.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "region")
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, name = "region_code")
    private String regionCode;

    @Column(name = "region_name")
    private String regionName;

    @ManyToOne
    @JsonBackReference
	@JoinColumn(name = "country_id")
    private Country country;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = District.class, mappedBy = "region", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<District> districts;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date updatedAt;

    @Transient
    private Long countryId;

    public Long getCountryId() {
        return this.getCountry().getId();
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    @Transient
    private String countryName;

    public String getCountryName() {
        return this.getCountry().getCountryName();
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Region(Long id, String regionCode, String regionName, Country country, Date createdAt, Date updatedAt) {
        this.id = id;
        this.regionCode = regionCode;
        this.regionName = regionName;
        this.country = country;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Region() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    @JsonIgnore
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country cCountry) {
        this.country = cCountry;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

}
