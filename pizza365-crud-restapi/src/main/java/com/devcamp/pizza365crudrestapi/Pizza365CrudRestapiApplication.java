package com.devcamp.pizza365crudrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza365CrudRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pizza365CrudRestapiApplication.class, args);
	}

}
