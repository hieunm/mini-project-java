package com.devcamp.pizza365crudrestapi.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityExistsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365crudrestapi.entities.Order;
import com.devcamp.pizza365crudrestapi.entities.User;
import com.devcamp.pizza365crudrestapi.errors.ResourceNotFoundException;
import com.devcamp.pizza365crudrestapi.repositories.OrderRepository;
import com.devcamp.pizza365crudrestapi.repositories.UserRepository;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Order getOrderById(Long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            return orderOptional.get();
        } else {
            return null;
        }
    }

    public Order createOrder(Order pOrder) {
        Optional<Order> orderOptional = orderRepository.findById(pOrder.getId());
        if (orderOptional.isPresent()) {
            throw new EntityExistsException("Đơn hàng đã tồn tại");
        } else {
            Optional<User> userOptional = userRepository.findById(pOrder.getUser().getId());
            if (userOptional.isPresent()) {
                pOrder.setUser(userOptional.get());
                pOrder.setCreatedAt(new Date());
                pOrder.setUpdatedAt(null);
                return orderRepository.save(pOrder);
            } else {
                throw new ResourceNotFoundException("Không tìm thấy user tương ứng");
            }
        }
    }

    public Order updateOrder(Long id, Order pOrder) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            //lấy order trong db để tránh cập nhật thiếu
            Order order = orderOptional.get();
            //check xem user có tồn tại ko
            Optional<User> userOptional = userRepository.findById(pOrder.getUser().getId());
            if (userOptional.isPresent()) {
                //thực hiện các thao tác logic
                order.setOrderCode(pOrder.getOrderCode());
                order.setPizzaSize(pOrder.getPizzaSize());
                order.setPizzaType(pOrder.getPizzaType());
                order.setPrice(pOrder.getPrice());
                order.setPaid(pOrder.getPaid());
                order.setUser(pOrder.getUser());
                order.setUpdatedAt(new Date());

                return orderRepository.save(order);
            } else {
                throw new ResourceNotFoundException("Không tìm thấy user tương ứng");
            }
        } else {
            throw new ResourceNotFoundException("Đơn hàng không tồn tại");
        }
    }

    public void deleteOrder(Long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            orderRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException("Đơn hàng không tồn tại");
        }
    }
}
