package com.devcamp.pizza365crudrestapi.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365crudrestapi.entities.Menu;
import com.devcamp.pizza365crudrestapi.errors.ResourceNotFoundException;
import com.devcamp.pizza365crudrestapi.repositories.MenuRepository;

@Service
public class MenuService {
    @Autowired
    private MenuRepository menuRepository;

    public List<Menu> getAllMenu() {
        return menuRepository.findAll();
    }

    public Menu GetMenuById(Long id) {
        return menuRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy menu"));
    }

    public Menu createMenu(Menu pMenu) {
        menuRepository.findById(pMenu.getId())
                .ifPresent(menu -> {
                    throw new ResourceNotFoundException("Menu đã tồn tại");
                });
        pMenu.setCreatedAt(new Date());
        pMenu.setUpdatedAt(null);
        return menuRepository.save(pMenu);// order ko cần check vì nếu chưa tồn tại thì tạo mới luôn
    }

    public Menu updateMenu(Long id, Menu pMenu) {
        Menu menu = menuRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy menu"));

        menu.setKichCo(pMenu.getKichCo());
        menu.setDuongKinh(pMenu.getDuongKinh());
        menu.setNuocNgot(pMenu.getNuocNgot());
        menu.setSalad(pMenu.getSalad());
        menu.setSuonNuong(pMenu.getSuonNuong());
        menu.setThanhTien(pMenu.getThanhTien());
        menu.setUpdatedAt(new Date());

        return menuRepository.save(menu);
    }

    public void deleteMenu(Long id) {
        menuRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy menu"));
        menuRepository.deleteById(id);
    }
}
