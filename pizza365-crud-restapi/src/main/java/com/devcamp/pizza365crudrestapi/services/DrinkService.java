package com.devcamp.pizza365crudrestapi.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365crudrestapi.entities.Drink;
import com.devcamp.pizza365crudrestapi.repositories.DrinkRepository;

@Service
public class DrinkService {
    @Autowired
    private DrinkRepository drinkRepository;

    public List<Drink> getAllDrinks() {
        return drinkRepository.findAll();//findAll trả về 1 List
    }

    public Drink findDrinkById(long id) {
        Optional<Drink> drink = drinkRepository.findById(id);
        if (drink.isPresent()) {
            return drink.get();
        } else {
            return null;
        }
    }

    public Drink createDrink(Drink pDrink) {
        pDrink.setNgayTao(new Date());
        pDrink.setNgayCapNhat(null);
        Drink drink = drinkRepository.save(pDrink);
        return drink;
    }

    public Drink updateDrinkById(long id, Drink pDrink) {
        Optional<Drink> drinkData = drinkRepository.findById(id);
        if (drinkData.isPresent()) {
            Drink drink = drinkData.get();
            drink.setMaNuocUong(pDrink.getMaNuocUong());
            drink.setTenNuocUong(pDrink.getTenNuocUong());
            drink.setPrice(pDrink.getPrice());
            drink.setGhiChu(pDrink.getGhiChu());
            drink.setNgayCapNhat(new Date());
            return drinkRepository.save(drink);
        } else {
            return null;
        }
    }
    
	public void deleteDrinkById(long id) {
		drinkRepository.deleteById(id);
	}

	public void deleteAllDrink() {
		drinkRepository.deleteAll();
	}
}
