package com.devcamp.pizza365crudrestapi.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365crudrestapi.errors.ResourceNotFoundException;
import com.devcamp.pizza365crudrestapi.entities.Country;
import com.devcamp.pizza365crudrestapi.repositories.CountryRepository;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepository;

    public Country createCountry(Country cCountry) {
        Optional<Country> countryOptional = countryRepository.findById(cCountry.getId());
        if (countryOptional.isPresent()) {
            Country newRole = new Country();
            newRole.setCountryName(cCountry.getCountryName());
            newRole.setCountryCode(cCountry.getCountryCode());
            newRole.setCreatedAt(new Date());
            newRole.setUpdatedAt(null);
            Country savedRole = countryRepository.save(newRole);
            return savedRole;
        } else {
            return null;
        }
    }

    public Country updateCountry(Long id, Country cCountry) {
        Optional<Country> countryData = countryRepository.findById(id);
        if (countryData.isPresent()) {
            Country newCountry = countryData.get();
            newCountry.setCountryName(cCountry.getCountryName());
            newCountry.setCountryCode(cCountry.getCountryCode());
            newCountry.setUpdatedAt(new Date());
            Country savedCountry = countryRepository.save(newCountry);
            return savedCountry;
        } else {
            return null;
        }
    }

    public void deleteCountryById(Long id) {
        Optional<Country> optional = countryRepository.findById(id);
        if (optional.isPresent()) {
            countryRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException("Không tìm thấy country tương ứng");
        }
    }

    public Country getCountryById(Long id) {
        if (countryRepository.findById(id).isPresent())
            return countryRepository.findById(id).get();
        else
            return null;
    }

    public List<Country> getAllCountry() {
        return countryRepository.findAll();
    }

    public Long countCountry() {
        return countryRepository.count();
    }

    public Boolean checkCountryById(Long id) {
        return countryRepository.existsById(id);
    }

    public List<Country> getCountryPagination(String page, String size) {
        Pageable pageWithPagination = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        Page<Country> countryPage = countryRepository.findAll(pageWithPagination);
        return countryPage.getContent();
    }
}
