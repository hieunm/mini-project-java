package com.devcamp.pizza365crudrestapi.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.devcamp.pizza365crudrestapi.entities.District;
import com.devcamp.pizza365crudrestapi.entities.Region;
import com.devcamp.pizza365crudrestapi.repositories.DistrictRepository;
import com.devcamp.pizza365crudrestapi.repositories.RegionRepository;

@Service
public class DistrictService {
    @Autowired
    private DistrictRepository districtRepository;
    @Autowired
    private RegionRepository regionRepository;

    public District createDistrict(District cDistrict) {
		Optional<Region> regionData = regionRepository.findById(cDistrict.getRegion().getId());
			if (regionData.isPresent()) {
                District newRole = new District();
                newRole.setDistrictName(cDistrict.getDistrictName());
                newRole.setDistrictCode(cDistrict.getDistrictCode());
                
                Region _region = regionData.get();
                newRole.setRegion(_region);

				newRole.setCreatedAt(new Date());
				newRole.setUpdatedAt(null);
                
                District savedRole = districtRepository.save(newRole);
                return savedRole;
			} else {
                return null;
            }
	}

    public District updateDistrict(Long id, District cDistrict) {
		Optional<District> districtData = districtRepository.findById(id);
		if (districtData.isPresent()) {
			District newDistrict = districtData.get();
			newDistrict.setDistrictCode(cDistrict.getDistrictCode());
			newDistrict.setDistrictName(cDistrict.getDistrictName());
			newDistrict.setUpdatedAt(new Date());
			District savedDistrict = districtRepository.save(newDistrict);
			return savedDistrict;
		} else {
			return null;
		}
	}

    public void deleteDistrictById(@PathVariable Long id) {
		districtRepository.deleteById(id);
	}

    public District getDistrictById(@PathVariable Long id) {
		if (districtRepository.findById(id).isPresent())
			return districtRepository.findById(id).get();
		else
			return null;
	}

    public List<District> getAllDistrict() {
		return districtRepository.findAll();
	}

}
