package com.devcamp.pizza365crudrestapi.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365crudrestapi.entities.User;
import com.devcamp.pizza365crudrestapi.entities.Order;
import com.devcamp.pizza365crudrestapi.errors.ResourceNotFoundException;
import com.devcamp.pizza365crudrestapi.repositories.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Set<Order> getOrdersByUserId(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            return userOptional.get().getOrders();
        } else {
            return null;
        }
    }

    public Order getInfoOrderOfUserById(Long userId, Long orderId) {
        // Tìm kiếm thông tin người dùng
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy user"));
        // Lấy danh sách đơn hàng của người dùng
        Set<Order> orders = user.getOrders();
        // Tìm kiếm đơn hàng tương ứng với orderId
        Optional<Order> optionalOrder = orders.stream()
                .filter(order -> Long.valueOf(order.getId()).equals(orderId.longValue()))//chuyển id sang Long để so sánh với orderId
                .findFirst();
        //Ko thấy thì ném exception, thấy thì trả về đối tượng
        Order order = optionalOrder.orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy order tương ứng trong user"));
        return order;
    }

    public User getUserById(Long id) {
        Optional<User> userFound = userRepository.findById(id);
        if (userFound.isPresent()) {
            return userFound.get();
        } else {
            return null;
        }
    }

    public User createUser(User pUser) {
        Optional<User> userFound = userRepository.findById(pUser.getId());
        if (userFound.isPresent()) {
            return null;
        } else {
            pUser.setCreatedAt(new Date());
            pUser.setUpdatedAt(null);
            return userRepository.save(pUser);// order ko cần check vì nếu chưa tồn tại thì tạo mới luôn
        }
    }

    public User updateUser(Long id, User pUser) {
        Optional<User> userFound = userRepository.findById(id);
        if (userFound.isPresent()) {
            User user = userFound.get();

            user.setFullName(pUser.getFullName());
            user.setEmail(pUser.getEmail());
            user.setPhone(pUser.getPhone());
            user.setAddress(pUser.getAddress());
            user.setOrders(pUser.getOrders());
            user.setUpdatedAt(new Date());

            return userRepository.save(user);
        } else {
            return null;
        }
    }

    public void deleteUser(Long id) {
        Optional<User> userFound = userRepository.findById(id);
        if (userFound.isPresent()) {
            userRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException("Không tìm thấy user tương ứng");
        }
    }

    public Long countUser() {
        return userRepository.count();
    }

    public Boolean checkUserById(Long id) {
        return userRepository.existsById(id);
    }

    public List<User> getUserPagination(String page, String size) {
        Pageable pageWithPagination = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        Page<User> userPage = userRepository.findAll(pageWithPagination);
        return userPage.getContent();
    }
}
