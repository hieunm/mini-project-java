package com.devcamp.pizza365crudrestapi.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.devcamp.pizza365crudrestapi.entities.Country;
import com.devcamp.pizza365crudrestapi.entities.Region;
import com.devcamp.pizza365crudrestapi.repositories.CountryRepository;
import com.devcamp.pizza365crudrestapi.repositories.RegionRepository;

@Service
public class RegionService {
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private CountryRepository countryRepository;

    public Region createRegion(Region cRegion) {
		Optional<Country> countryData = countryRepository.findById(cRegion.getCountry().getId());
			if (countryData.isPresent()) {
                Region newRole = new Region();
                newRole.setRegionName(cRegion.getRegionName());
                newRole.setRegionCode(cRegion.getRegionCode());
                newRole.setCountry(cRegion.getCountry());
                
                Country _country = countryData.get();
                newRole.setCountry(_country);
				newRole.setCreatedAt(new Date());
				newRole.setUpdatedAt(null);
                
                Region savedRole = regionRepository.save(newRole);
                return savedRole;
			} else {
                return null;
            }
	}

    public Region updateRegion(Long id, Region cRegion) {
		Optional<Region> regionData = regionRepository.findById(id);
		if (regionData.isPresent()) {
			Region newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			newRegion.setUpdatedAt(new Date());
			Region savedRegion = regionRepository.save(newRegion);
			return savedRegion;
		} else {
			return null;
		}
	}

    public void deleteRegionById(@PathVariable Long id) {
		regionRepository.deleteById(id);
	}

    public Region getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}

    public List<Region> getAllRegion() {
		return regionRepository.findAll();
	}

    public List<Region> getRegionsByCountry(Long countryId) {
		return regionRepository.findByCountryId(countryId);
	}

    public Optional<Region> getRegionByRegionAndCountry(Long countryId, Long regionId) {
		return regionRepository.findByIdAndCountryId(countryId, regionId);
	}

	public Integer countRegionByCountryId(Long countryId) {
		return regionRepository.findByCountryId(countryId).size();
	}

	public Boolean checkRegionById(Long id) {
		return regionRepository.existsById(id);
	}
}
