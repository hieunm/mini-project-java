package com.devcamp.pizza365crudrestapi.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.devcamp.pizza365crudrestapi.entities.Voucher;
import com.devcamp.pizza365crudrestapi.repositories.VoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class VoucherService {
    @Autowired
    VoucherRepository pVoucherRepository;

    public ArrayList<Voucher> getVoucherList() {
        ArrayList<Voucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }

    public Voucher findVoucherById(long id) {
        Optional<Voucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
            return voucherData.get();
        } else {
            return null;
        }
    }

    public Voucher createCVoucher(Voucher pVouchers) {
        pVouchers.setNgayTao(new Date());
        pVouchers.setNgayCapNhat(null);
        Voucher _vouchers = pVoucherRepository.save(pVouchers);
        return _vouchers;
    }

    public Voucher updateCVoucherById(long id, Voucher pVouchers) {
        Optional<Voucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
            Voucher voucher = voucherData.get();
            voucher.setMaVoucher(pVouchers.getMaVoucher());
            voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
            voucher.setGhiChu(pVouchers.getGhiChu());
            voucher.setNgayCapNhat(new Date());
            return pVoucherRepository.save(voucher);
        } else {
            return null;
        }
    }

	public void deleteCVoucherById(long id) {
		pVoucherRepository.deleteById(id);
	}

	public void deleteAllCVoucher() {
		pVoucherRepository.deleteAll();
	}
}
