package com.devcamp.pizza365crudrestapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365crudrestapi.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
