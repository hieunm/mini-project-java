package com.devcamp.pizza365crudrestapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365crudrestapi.entities.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
	Country findByCountryCode(String countryCode);

}
