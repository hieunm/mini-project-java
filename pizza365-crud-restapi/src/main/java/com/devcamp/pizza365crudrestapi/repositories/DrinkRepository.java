package com.devcamp.pizza365crudrestapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365crudrestapi.entities.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Long> {

}
