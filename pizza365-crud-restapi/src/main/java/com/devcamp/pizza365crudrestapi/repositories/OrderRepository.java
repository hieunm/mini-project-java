package com.devcamp.pizza365crudrestapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365crudrestapi.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
