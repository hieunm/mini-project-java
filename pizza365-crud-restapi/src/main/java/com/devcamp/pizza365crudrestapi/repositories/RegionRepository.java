package com.devcamp.pizza365crudrestapi.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365crudrestapi.entities.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {
	@Query("SELECT r FROM Region r WHERE r.country.id = :countryId")
	List<Region> findByCountryId(Long countryId);

	@Query("SELECT r FROM Region r WHERE r.country.id = :countryId AND r.id = :id")
	Optional<Region> findByIdAndCountryId(Long countryId, Long id);
}
