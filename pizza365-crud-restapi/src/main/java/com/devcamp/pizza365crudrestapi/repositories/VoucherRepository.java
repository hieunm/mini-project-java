package com.devcamp.pizza365crudrestapi.repositories;

import com.devcamp.pizza365crudrestapi.entities.Voucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {
}
