$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_COUNTRIES = "http://localhost:8080/countries";
  var gId;

  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gCOLS = ["id", "countryCode", "countryName", "createdAt",  "updatedAt", "action"];

  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT_COL = 0;
  const gMA_QUOC_GIA_COL = 1;
  const gTEN_QUOC_GIA_COL = 2;
  const gNGAY_TAO_COL = 3;
  const gNGAY_CAP_NHAT_COL = 4;
  const gACTION_COL = 5;

  //định nghĩa table
  var gTable = $("#country-table").DataTable({
    columns: [
      { data: gCOLS[gSTT_COL] },
      { data: gCOLS[gMA_QUOC_GIA_COL] },
      { data: gCOLS[gTEN_QUOC_GIA_COL] },
      { data: gCOLS[gNGAY_TAO_COL] },
      { data: gCOLS[gNGAY_CAP_NHAT_COL] },
      { data: gCOLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        target: gSTT_COL,
        className: "text-center",
        render: (data, type, row, meta) => {
          return meta.row + 1;
        },
      },
      {
        target: gACTION_COL,
        className: "text-center",
        defaultContent: `
          <div class="d-flex justify-content-around">
            <i class="fas fa-edit update-icon btn text-primary mx-auto"></i>
            <i class="fas fa-trash-alt delete-icon btn text-danger mx-auto"></i>
          </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //Event click btn thêm mới
  $("#btn-create").click(() => {
    $("#create-modal").modal("show");
  });

  //Event click icon update
  $("#country-table").on("click", ".update-icon", function () {
    onIconUpdateClick(this);
  });

  //Event click icon delete
  $("#country-table").on("click", ".delete-icon", function () {
    onIconDeleteClick(this);
  });

  //Event click btn confirm create
  $("#btn-confirm-create").click(onBtnConfirmCreateClick);

  //Event click btn confirm update
  $("#btn-confirm-update").click(onBtnConfirmUpdateClick);

  //Event click btn confirm delete
  $("#btn-confirm-delete").click(callAPIDeleteCountryById);

  //Event hidden modal create
  $("#create-modal").on("hidden.bs.modal", clearFormModalCreate);

  //Event hidden modal update
  $("#update-modal").on("hidden.bs.modal", clearFormModalUpdate);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    callAPIGetAllCountries();
  }

  // Hàm xử lý sự kiện click icon update
  function onIconUpdateClick(paramUpdateIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramUpdateIcon);
    //B3: call API get by ID
    callAPIGetCountryById();
  }

  // Hàm xử lý sự kiện click icon delete
  function onIconDeleteClick(paramDeleteIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramDeleteIcon);
    $("#delete-modal").modal("show");
  }

  // Hàm xử lý sự kiện click nút confirm tạo mới
  function onBtnConfirmCreateClick() {
    // đối tượng sẽ được tạo mới
    var countryObj = {
      countryCode: "",
      countryName: "",
    };
    // B1: Thu thập dữ liệu
    collectDataCreate(countryObj);
    // B2: Validate
    let vCheck = validationData(countryObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPICreateCountry(countryObj);
    }
  }

  // Hàm xử lý sự kiện click nút confirm update
  function onBtnConfirmUpdateClick() {
    // đối tượng sẽ được tạo mới
    var countryObj = {
      countryCode: "",
      countryName: "",
    };
    // B1: Thu thập dữ liệu
    collectDataUpdate(countryObj);
    // B2: Validate
    let vCheck = validationData(countryObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPIUpdateCountryById(countryObj);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllCountries() {
    $.ajax({
      url: URL_API_COUNTRIES,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToTable(res);
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPICreateCountry(paramCountry) {
    $.ajax({
      url: URL_API_COUNTRIES,
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramCountry),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Tạo mới thành công");
        $("#create-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Tạo mới thất bại");
      },
    });
  }

  function callAPIGetCountryById() {
    $.ajax({
      url: URL_API_COUNTRIES + "/" + gId,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUpdateForm(res);
        $("#update-modal").modal("show");
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIUpdateCountryById(province) {
    $.ajax({
      url: URL_API_COUNTRIES + "/" + gId,
      method: "PUT",
      contentType: "application/json",
      data: JSON.stringify(province),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Cập nhật thành công");
        $("#update-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Cập nhật thất bại");
      },
    });
  }

  function callAPIDeleteCountryById() {
    $.ajax({
      url: URL_API_COUNTRIES + "/" + gId,
      method: "DELETE",
      success: function (res) {
        console.log(res);
        showToast(1, "Xóa thành công");
        $("#delete-modal").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Xóa thất bại");
      },
    });
  }

  //handle function
  function loadDataToTable(paramResponse) {
    gTable.clear();
    gTable.rows.add(paramResponse);
    gTable.draw();
  }

  function loadDataToUpdateForm(paramResponse) {
    gId = paramResponse.id;
    $("#input-code-update").val(paramResponse.countryCode);
    $("#input-name-update").val(paramResponse.countryName);
    let getNgayTao = new Date(convertDateFormat(paramResponse.ngayTao));
    $("#input-created-at-update").val(getNgayTao.toLocaleDateString("en-CA")); //en-CA có dạng yyyy-MM-dd
    let getNgayCapNhat = new Date(convertDateFormat(paramResponse.ngayCapNhat));
    $("#input-updated-at-update").val(getNgayCapNhat.toLocaleDateString("en-CA"));
  }

  // Hàm thu thập thông tin về ID của row mà icon đc click
  function collectIdRowClick(paramIcon) {
    let vRowClick = $(paramIcon).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gId = vRowData.id; //lưu id
  }

  function collectDataCreate(paramCountry) {
    paramCountry.countryCode = $("#input-code-create").val();
    paramCountry.countryName = $("#input-name-create").val();
  }

  function collectDataUpdate(paramCountry) {
    paramCountry.countryCode = $("#input-code-update").val();
    paramCountry.countryName = $("#input-name-update").val();
  }

  function validationData(paramCountry) {
    if (paramCountry.countryCode === "") {
      showToast(3, "Vui lòng nhập mã quốc gia");
      return false;
    }
    if (paramCountry.countryName === "") {
      showToast(3, "Vui lòng nhập tên quốc gia");
      return false;
    }
    return true;
  }

  //chuyển ngày dd-MM-yyyy thành yyyy-MM-dd(để trình duyệt hiểu)
  function convertDateFormat(paramDateString) {
    if (paramDateString) {
      const dateParts = paramDateString.split("-");
      return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }
  }

  // Hàm xử lý sự kiện hidden modal create
  function clearFormModalCreate() {
    $("#input-code-create").val("");
    $("#input-name-create").val("");
  }

  // Hàm xử lý sự kiện hidden modal update
  function clearFormModalUpdate() {
    $("#input-code-update").val("");
    $("#input-name-update").val("");
    $("#input-created-at-update").val("");
    $("#input-updated-at-update").val("");
  }

  // Hàm hiển thị thông báo
  function showToast(paramType, paramMessage) {
    switch (paramType) {
      case 1: //success
        toastr.success(paramMessage);
        break;
      case 2: //info
        toastr.info(paramMessage);
        break;
      case 3: //error
        toastr.error(paramMessage);
        break;
      case 4: //warning
        toastr.warning(paramMessage);
        break;
    }
  }
});
