$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_DRINKS = "http://localhost:8080/drinks";
  var gId;

  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gCOLS = ["id", "maNuocUong", "tenNuocUong", "price", "ghiChu", "ngayTao",  "ngayCapNhat", "action"];

  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT_COL = 0;
  const gMA_NUOC_UONG_COL = 1;
  const gTEN_NUOC_UONG_COL = 2;
  const gPRICE_COL = 3;
  const gGHI_CHU_COL = 4;
  const gNGAY_TAO_COL = 5;
  const gNGAY_CAP_NHAT_COL = 6;
  const gACTION_COL = 7;

  //định nghĩa table
  var gTable = $("#drink-table").DataTable({
    columns: [
      { data: gCOLS[gSTT_COL] },
      { data: gCOLS[gMA_NUOC_UONG_COL] },
      { data: gCOLS[gTEN_NUOC_UONG_COL] },
      { data: gCOLS[gPRICE_COL] },
      { data: gCOLS[gGHI_CHU_COL] },
      { data: gCOLS[gNGAY_TAO_COL] },
      { data: gCOLS[gNGAY_CAP_NHAT_COL] },
      { data: gCOLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        target: gSTT_COL,
        className: "text-center",
        render: (data, type, row, meta) => {
          return meta.row + 1;
        },
      },
      {
        target: gACTION_COL,
        className: "text-center",
        defaultContent: `
          <div class="d-flex justify-content-around">
            <i class="fas fa-edit update-icon btn text-primary mx-auto"></i>
            <i class="fas fa-trash-alt delete-icon btn text-danger mx-auto"></i>
          </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //Event click btn thêm mới
  $("#btn-create").click(() => {
    $("#create-modal").modal("show");
  });

  //Event click icon update
  $("#drink-table").on("click", ".update-icon", function () {
    onIconUpdateClick(this);
  });

  //Event click icon delete
  $("#drink-table").on("click", ".delete-icon", function () {
    onIconDeleteClick(this);
  });

  //Event click btn confirm create
  $("#btn-confirm-create").click(onBtnConfirmCreateClick);

  //Event click btn confirm update
  $("#btn-confirm-update").click(onBtnConfirmUpdateClick);

  //Event click btn confirm delete
  $("#btn-confirm-delete").click(callAPIDeleteMenuById);

  //Event hidden modal create
  $("#create-modal").on("hidden.bs.modal", clearFormModalCreate);

  //Event hidden modal update
  $("#update-modal").on("hidden.bs.modal", clearFormModalUpdate);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    callAPIGetAllDrinks();
  }

  // Hàm xử lý sự kiện click icon update
  function onIconUpdateClick(paramUpdateIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramUpdateIcon);
    //B3: call API get by ID
    callAPIGetDrinkById();
  }

  // Hàm xử lý sự kiện click icon delete
  function onIconDeleteClick(paramDeleteIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramDeleteIcon);
    $("#delete-modal").modal("show");
  }

  // Hàm xử lý sự kiện click nút confirm tạo mới
  function onBtnConfirmCreateClick() {
    // đối tượng sẽ được tạo mới
    var drinkObj = {
      maNuocUong: "",
      tenNuocUong: "",
      price: -1,
      ghiChu: "",
    };
    // B1: Thu thập dữ liệu
    collectDataCreate(drinkObj);
    // B2: Validate
    let vCheck = validationData(drinkObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPICreateDrink(drinkObj);
    }
  }

  // Hàm xử lý sự kiện click nút confirm update
  function onBtnConfirmUpdateClick() {
    // đối tượng sẽ được tạo mới
    var drinkObj = {
      maNuocUong: "",
      tenNuocUong: "",
      price: -1,
      ghiChu: "",
    };
    // B1: Thu thập dữ liệu
    collectDataUpdate(drinkObj);
    // B2: Validate
    let vCheck = validationData(drinkObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPIUpdateDrinkById(drinkObj);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllDrinks() {
    $.ajax({
      url: URL_API_DRINKS,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToTable(res);
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPICreateDrink(paramDrink) {
    $.ajax({
      url: URL_API_DRINKS,
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramDrink),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Tạo mới thành công");
        $("#create-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Tạo mới thất bại");
      },
    });
  }

  function callAPIGetDrinkById() {
    $.ajax({
      url: URL_API_DRINKS + "/" + gId,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUpdateForm(res);
        $("#update-modal").modal("show");
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIUpdateDrinkById(province) {
    $.ajax({
      url: URL_API_DRINKS + "/" + gId,
      method: "PUT",
      contentType: "application/json",
      data: JSON.stringify(province),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Cập nhật thành công");
        $("#update-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Cập nhật thất bại");
      },
    });
  }

  function callAPIDeleteMenuById() {
    $.ajax({
      url: URL_API_DRINKS + "/" + gId,
      method: "DELETE",
      success: function (res) {
        console.log(res);
        showToast(1, "Xóa thành công");
        $("#delete-modal").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Xóa thất bại");
      },
    });
  }

  //handle function
  function loadDataToTable(paramResponse) {
    gTable.clear();
    gTable.rows.add(paramResponse);
    gTable.draw();
  }

  function loadDataToUpdateForm(paramResponse) {
    gId = paramResponse.id;
    $("#input-code-update").val(paramResponse.maNuocUong);
    $("#input-name-update").val(paramResponse.tenNuocUong);
    $("#input-note-update").val(paramResponse.ghiChu);
    $("#input-price-update").val(paramResponse.price);
    let getNgayTao = new Date(convertDateFormat(paramResponse.ngayTao));
    $("#input-created-at-update").val(getNgayTao.toLocaleDateString("en-CA")); //en-CA có dạng yyyy-MM-dd
    let getNgayCapNhat = new Date(convertDateFormat(paramResponse.ngayCapNhat));
    $("#input-updated-at-update").val(getNgayCapNhat.toLocaleDateString("en-CA"));
  }

  // Hàm thu thập thông tin về ID của row mà icon đc click
  function collectIdRowClick(paramIcon) {
    let vRowClick = $(paramIcon).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gId = vRowData.id; //lưu id
  }

  function collectDataCreate(paramDrink) {
    paramDrink.maNuocUong = $("#input-code-create").val();
    paramDrink.tenNuocUong = $("#input-name-create").val();
    paramDrink.ghiChu = $("#input-note-create").val();
    paramDrink.price = parseInt($("#input-price-create").val());
  }

  function collectDataUpdate(paramDrink) {
    paramDrink.maNuocUong = $("#input-code-update").val();
    paramDrink.tenNuocUong = $("#input-name-update").val();
    paramDrink.ghiChu = $("#input-note-update").val();
    paramDrink.price = parseInt($("#input-price-update").val());
  }

  function validationData(paramDrink) {
    if (paramDrink.maNuocUong === "") {
      showToast(3, "Vui lòng nhập mã nước uống");
      return false;
    }
    if (paramDrink.maNuocUong === "") {
      showToast(3, "Vui lòng nhập tên nước uống");
      return false;
    }
    if (paramDrink.price < 0) {
      showToast(3, "Số tiền phải >= 0");
      return false;
    }
    return true;
  }

  //chuyển ngày dd-MM-yyyy thành yyyy-MM-dd(để trình duyệt hiểu)
  function convertDateFormat(paramDateString) {
    if (paramDateString) {
      const dateParts = paramDateString.split("-");
      return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }
  }

  // Hàm xử lý sự kiện hidden modal create
  function clearFormModalCreate() {
    $("#input-code-create").val("");
    $("#input-name-create").val("");
    $("#input-note-create").val("");
    $("#input-price-create").val("");
  }

  // Hàm xử lý sự kiện hidden modal update
  function clearFormModalUpdate() {
    $("#input-code-update").val("");
    $("#input-name-update").val("");
    $("#input-note-update").val("");
    $("#input-price-update").val("");
    $("#input-created-at-update").val("");
    $("#input-updated-at-update").val("");
  }

  // Hàm hiển thị thông báo
  function showToast(paramType, paramMessage) {
    switch (paramType) {
      case 1: //success
        toastr.success(paramMessage);
        break;
      case 2: //info
        toastr.info(paramMessage);
        break;
      case 3: //error
        toastr.error(paramMessage);
        break;
      case 4: //warning
        toastr.warning(paramMessage);
        break;
    }
  }
});
