$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_REGIONS = "http://localhost:8080/regions";
  var URL_API_COUNTRIES = "http://localhost:8080/countries";
  var gRegionId; //update, delete
  var gCountries; //mảng countries dùng để load countries vào select
  var gCountryId; //countryId dùng để load countries vào select

  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gCOLS = [
    "id",
    "regionCode",
    "regionName",
    "countryName",
    "createdAt",
    "updatedAt",
    "action",
  ];

  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT_COL = 0;
  const gREGION_CODE_COL = 1;
  const gREGION_NAME_COL = 2;
  const gCOUNTRY_NAME_COL = 3;
  const gCREATE_DAY_COL = 4;
  const gUPDATE_DAY_COL = 5;
  const gACTION_COL = 6;

  //định nghĩa table
  var gTable = $("#region-table").DataTable({
    columns: [
      { data: gCOLS[gSTT_COL] },
      { data: gCOLS[gREGION_CODE_COL] },
      { data: gCOLS[gREGION_NAME_COL] },
      { data: gCOLS[gCOUNTRY_NAME_COL] },
      { data: gCOLS[gCREATE_DAY_COL] },
      { data: gCOLS[gUPDATE_DAY_COL] },
      { data: gCOLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        target: gSTT_COL,
        className: "text-center",
        render: (data, type, row, meta) => {
          return meta.row + 1;
        },
      },
      {
        target: gACTION_COL,
        className: "text-center",
        defaultContent: `
          <div class="d-flex justify-content-around">
            <i class="fas fa-edit update-icon btn text-primary mx-auto"></i>
            <i class="fas fa-trash-alt delete-icon btn text-danger mx-auto"></i>
          </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //Event show modal
  $("#create-modal, #update-modal").on("shown.bs.modal", function () {
    //load select user
    loadDataToCountrySelect(gCountries);
    // Kiểm tra xem modal hiện tại có id là "update-modal" hay không
    if ($(this).attr("id") === "update-modal") {
      //gán user vào select update
      $("#select-country-update").val(gCountryId);
    }
  });

  //Event click btn thêm mới
  $("#btn-create").click(() => {
    $("#create-modal").modal("show");
  });

  //Event click icon update
  $("#region-table").on("click", ".update-icon", function () {
    onIconUpdateClick(this);
  });

  //Event click icon delete
  $("#region-table").on("click", ".delete-icon", function () {
    onIconDeleteClick(this);
  });

  //Event click btn confirm create
  $("#btn-confirm-create").click(onBtnConfirmCreateClick);

  //Event click btn confirm update
  $("#btn-confirm-update").click(onBtnConfirmUpdateClick);

  //Event click btn confirm delete
  $("#btn-confirm-delete").click(callAPIDeleteRegionById);

  //Event hidden modal create
  $("#create-modal").on("hidden.bs.modal", clearFormModalCreate);

  //Event hidden modal update
  $("#update-modal").on("hidden.bs.modal", clearFormModalUpdate);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    callAPIGetAllCountries();
    callAPIGetAllRegions();
  }

  // Hàm xử lý sự kiện click icon update
  function onIconUpdateClick(paramUpdateIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramUpdateIcon);
    //B3: call API get by ID
    callAPIGetRegionById();
  }

  // Hàm xử lý sự kiện click icon delete
  function onIconDeleteClick(paramDeleteIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramDeleteIcon);
    $("#delete-modal").modal("show");
  }

  // Hàm xử lý sự kiện click nút confirm tạo mới
  function onBtnConfirmCreateClick() {
    // đối tượng sẽ được tạo mới
    var regionObj = {
      regionCode: "",
      regionName: "",
      country: {
        id: "",
      },
    };
    // B1: Thu thập dữ liệu
    collectDataCreate(regionObj);
    // B2: Validate
    let vCheck = validationData(regionObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPICreateRegion(regionObj);
    }
  }

  // Hàm xử lý sự kiện click nút confirm update
  function onBtnConfirmUpdateClick() {
    // đối tượng sẽ được tạo mới
    var regionObj = {
      regionCode: "",
      regionName: "",
      country: {
        id: "",
      },
    };
    // B1: Thu thập dữ liệu
    collectDataUpdate(regionObj);
    // B2: Validate
    let vCheck = validationData(regionObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPIUpdateRegionById(regionObj);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllRegions() {
    $.ajax({
      url: URL_API_REGIONS,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToTable(res);
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIGetAllCountries() {
    $.ajax({
      url: URL_API_COUNTRIES,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToCountrySelect(res);
        gCountries = res;
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIGetRegionById() {
    $.ajax({
      url: URL_API_REGIONS + "/" + gRegionId,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUpdateForm(res);
        $("#update-modal").modal("show");
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPICreateRegion(paramOrder) {
    $.ajax({
      url: URL_API_REGIONS,
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramOrder),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Tạo mới thành công");
        $("#create-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Tạo mới thất bại");
      },
    });
  }

  function callAPIUpdateRegionById(paramOrder) {
    $.ajax({
      url: URL_API_REGIONS + "/" + gRegionId,
      method: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramOrder),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Cập nhật thành công");
        $("#update-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Cập nhật thất bại");
      },
    });
  }

  function callAPIDeleteRegionById() {
    $.ajax({
      url: URL_API_REGIONS + "/" + gRegionId,
      method: "DELETE",
      success: function (res) {
        console.log(res);
        showToast(1, "Xóa thành công");
        $("#delete-modal").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Xóa thất bại");
      },
    });
  }

  //handle function
  function loadDataToTable(paramResponse) {
    gTable.clear();
    gTable.rows.add(paramResponse);
    gTable.draw();
  }

  function loadDataToUpdateForm(paramResponse) {
    gCountryId = paramResponse.countryId;
    gRegionId = paramResponse.id;
    $("#input-code-update").val(paramResponse.regionCode);
    $("#input-name-update").val(paramResponse.regionName);
    let getNgayTao = new Date(convertDateFormat(paramResponse.createdAt));
    $("#input-created-at-update").val(getNgayTao.toLocaleDateString("en-CA")); //en-CA có dạng yyyy-MM-dd
    let getNgayCapNhat = new Date(convertDateFormat(paramResponse.updatedAt));
    $("#input-updated-at-update").val(
      getNgayCapNhat.toLocaleDateString("en-CA")
    );
    //form select country sẽ gán ở sự kiện show modal update
  }

  function loadDataToCountrySelect(paramUsers) {
    $(".country-select").html("");
    for (let i = 0; i < paramUsers.length; i++) {
      let option = $("<option/>");
      option.prop("value", paramUsers[i].id);
      option.prop("text", paramUsers[i].countryName);
      $(".country-select").append(option);
    }
  }

  // collect function
  function collectIdRowClick(paramIcon) {
    let vRowClick = $(paramIcon).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gRegionId = vRowData.id; //lưu id
  }

  function collectDataCreate(paramRegion) {
    paramRegion.regionCode = $("#input-code-create").val();
    paramRegion.regionName = $("#input-name-create").val();
    paramRegion.country.id = $("#select-country-create").val();
  }

  function collectDataUpdate(paramOrder) {
    paramOrder.regionCode = $("#input-code-update").val();
    paramOrder.regionName = $("#input-name-update").val();
    paramOrder.country.id = $("#select-country-update").val();
  }

  function validationData(paramOrder) {
    if (paramOrder.regionCode === "") {
      showToast(3, "Vui lòng nhập mã vùng");
      return false;
    }
    if (paramOrder.regionName === "") {
      showToast(3, "Vui lòng nhập tên vùng");
      return false;
    }
    if (paramOrder.country.id === "") {
      showToast(3, "Vui lòng chọn quốc gia");
      return false;
    }
    return true;
  }

  //chuyển ngày dd-MM-yyyy thành yyyy-MM-dd(để trình duyệt hiểu)
  function convertDateFormat(paramDateString) {
    if (paramDateString) {
      const dateParts = paramDateString.split("-");
      return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }
  }

  // Hàm xử lý sự kiện hidden modal create
  function clearFormModalCreate() {
    $("#input-code-create").val("");
    $("#input-name-create").val("");
    $("#select-country-create").val("");
  }

  // Hàm xử lý sự kiện hidden modal update
  function clearFormModalUpdate() {
    $("#input-code-update").val("");
    $("#input-name-update").val("");
    $("#select-country-update").val("");
    $("#input-created-at-update").val("");
    $("#input-updated-at-update").val("");
  }

  // Hàm hiển thị thông báo
  function showToast(paramType, paramMessage) {
    switch (paramType) {
      case 1: //success
        toastr.success(paramMessage);
        break;
      case 2: //info
        toastr.info(paramMessage);
        break;
      case 3: //error
        toastr.error(paramMessage);
        break;
      case 4: //warning
        toastr.warning(paramMessage);
        break;
    }
  }
});
