$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_MENU = "http://localhost:8080/menu";
  var gId;

  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gCOLS = ["id", "kichCo", "duongKinh", "suonNuong", "salad", "nuocNgot",  "thanhTien", "createdAt", "updatedAt", "action"];

  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT_COL = 0;
  const gKICH_CO_COL = 1;
  const gDUONG_KINH_COL = 2;
  const gSUON_NUONG_COL = 3;
  const gSALAD_COL = 4;
  const gNUOC_NGOT_COL = 5;
  const gTHANH_TIEN_COL = 6;
  const gCREATE_DAY_COL = 7;
  const gUPDATE_DAY_COL = 8;
  const gACTION_COL = 9;

  //định nghĩa table
  var gTable = $("#menu-table").DataTable({
    columns: [
      { data: gCOLS[gSTT_COL] },
      { data: gCOLS[gKICH_CO_COL] },
      { data: gCOLS[gDUONG_KINH_COL] },
      { data: gCOLS[gSUON_NUONG_COL] },
      { data: gCOLS[gSALAD_COL] },
      { data: gCOLS[gNUOC_NGOT_COL] },
      { data: gCOLS[gTHANH_TIEN_COL] },
      { data: gCOLS[gCREATE_DAY_COL] },
      { data: gCOLS[gUPDATE_DAY_COL] },
      { data: gCOLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        target: gSTT_COL,
        className: "text-center",
        render: (data, type, row, meta) => {
          return meta.row + 1;
        },
      },
      {
        target: gACTION_COL,
        className: "text-center",
        defaultContent: `
          <div class="d-flex justify-content-around">
            <i class="fas fa-edit update-icon btn text-primary mx-auto"></i>
            <i class="fas fa-trash-alt delete-icon btn text-danger mx-auto"></i>
          </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //Event click btn thêm mới
  $("#btn-create").click(() => {
    $("#create-modal").modal("show");
  });

  //Event click icon update
  $("#menu-table").on("click", ".update-icon", function () {
    onIconUpdateClick(this);
  });

  //Event click icon delete
  $("#menu-table").on("click", ".delete-icon", function () {
    onIconDeleteClick(this);
  });

  //Event click btn confirm create
  $("#btn-confirm-create").click(onBtnConfirmCreateClick);

  //Event click btn confirm update
  $("#btn-confirm-update").click(onBtnConfirmUpdateClick);

  //Event click btn confirm delete
  $("#btn-confirm-delete").click(callAPIDeleteMenuById);

  //Event hidden modal create
  $("#create-modal").on("hidden.bs.modal", clearFormModalCreate);

  //Event hidden modal update
  $("#update-modal").on("hidden.bs.modal", clearFormModalUpdate);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    callAPIGetAllMenu();
  }

  // Hàm xử lý sự kiện click icon update
  function onIconUpdateClick(paramUpdateIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramUpdateIcon);
    //B3: call API get by ID
    callAPIGetMenuById();
  }

  // Hàm xử lý sự kiện click icon delete
  function onIconDeleteClick(paramDeleteIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramDeleteIcon);
    $("#delete-modal").modal("show");
  }

  // Hàm xử lý sự kiện click nút confirm tạo mới
  function onBtnConfirmCreateClick() {
    // đối tượng sẽ được tạo mới
    var menuObj = {
      kichCo: "",
      duongKinh: "",
      suonNuong: -1,
      salad: -1,
      nuocNgot: -1,
      thanhTien: -1,
    };
    // B1: Thu thập dữ liệu
    collectDataCreate(menuObj);
    // B2: Validate
    let vCheck = validationData(menuObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPICreateMenu(menuObj);
    }
  }

  // Hàm xử lý sự kiện click nút confirm update
  function onBtnConfirmUpdateClick() {
    // đối tượng sẽ được tạo mới
    var menuObj = {
      kichCo: "",
      duongKinh: "",
      suonNuong: -1,
      salad: -1,
      nuocNgot: -1,
      thanhTien: -1,
    };
    // B1: Thu thập dữ liệu
    collectDataUpdate(menuObj);
    // B2: Validate
    let vCheck = validationData(menuObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPIUpdateMenuById(menuObj);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllMenu() {
    $.ajax({
      url: URL_API_MENU,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToTable(res);
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPICreateMenu(paramMenu) {
    $.ajax({
      url: URL_API_MENU,
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramMenu),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Tạo mới thành công");
        $("#create-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Tạo mới thất bại");
      },
    });
  }

  function callAPIGetMenuById() {
    $.ajax({
      url: URL_API_MENU + "/" + gId,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUpdateForm(res);
        $("#update-modal").modal("show");
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIUpdateMenuById(province) {
    $.ajax({
      url: URL_API_MENU + "/" + gId,
      method: "PUT",
      contentType: "application/json",
      data: JSON.stringify(province),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Cập nhật thành công");
        $("#update-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Cập nhật thất bại");
      },
    });
  }

  function callAPIDeleteMenuById() {
    $.ajax({
      url: URL_API_MENU + "/" + gId,
      method: "DELETE",
      success: function (res) {
        console.log(res);
        showToast(1, "Xóa thành công");
        $("#delete-modal").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Xóa thất bại");
      },
    });
  }

  //handle function
  function loadDataToTable(paramResponse) {
    gTable.clear();
    gTable.rows.add(paramResponse);
    gTable.draw();
  }

  function loadDataToUpdateForm(paramResponse) {
    gId = paramResponse.id;
    $("#input-size-update").val(paramResponse.kichCo);
    $("#input-diameter-update").val(paramResponse.duongKinh);
    $("#input-ribs-update").val(paramResponse.suonNuong);
    $("#input-salad-update").val(paramResponse.salad);
    $("#input-drink-update").val(paramResponse.nuocNgot);
    $("#input-price-update").val(paramResponse.thanhTien);
    let getNgayTao = new Date(convertDateFormat(paramResponse.createdAt));
    $("#input-created-at-update").val(getNgayTao.toLocaleDateString("en-CA")); //en-CA có dạng yyyy-MM-dd
    let getNgayCapNhat = new Date(convertDateFormat(paramResponse.updatedAt));
    $("#input-updated-at-update").val(getNgayCapNhat.toLocaleDateString("en-CA"));
  }

  // Hàm thu thập thông tin về ID của row mà icon đc click
  function collectIdRowClick(paramIcon) {
    let vRowClick = $(paramIcon).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gId = vRowData.id; //lưu id
  }

  function collectDataCreate(paramMenu) {
    paramMenu.kichCo = $("#input-size-create").val();
    paramMenu.duongKinh = $("#input-diameter-create").val();
    paramMenu.suonNuong = parseInt($("#input-ribs-create").val());
    paramMenu.salad = parseInt($("#input-salad-create").val());
    paramMenu.nuocNgot = parseInt($("#input-drink-create").val());
    paramMenu.thanhTien = parseInt($("#input-price-create").val());
  }

  function collectDataUpdate(paramMenu) {
    paramMenu.kichCo = $("#input-size-update").val();
    paramMenu.duongKinh = $("#input-diameter-update").val();
    paramMenu.suonNuong = parseInt($("#input-ribs-update").val());
    paramMenu.salad = parseInt($("#input-salad-update").val());
    paramMenu.nuocNgot = parseInt($("#input-drink-update").val());
    paramMenu.thanhTien = parseInt($("#input-price-update").val());
  }

  function validationData(paramMenu) {
    if (paramMenu.kichCo === "") {
      showToast(3, "Vui lòng nhập kích cỡ");
      return false;
    }
    if (paramMenu.duongKinh === "") {
      showToast(3, "Vui lòng nhập đường kính");
      return false;
    }
    if (paramMenu.suonNuong < 0) {
      showToast(3, "Số lượng sườn nướng phải >= 0");
      return false;
    }
    if (paramMenu.salad < 0) {
      showToast(3, "Số lượng salad phải >= 0");
      return false;
    }
    if (paramMenu.nuocNgot < 0) {
      showToast(3, "Số lượng nước ngọt phải >= 0");
      return false;
    }
    if (paramMenu.thanhTien < 0) {
      showToast(3, "Số tiền phải >= 0");
      return false;
    }
    return true;
  }

  //chuyển ngày dd-MM-yyyy thành yyyy-MM-dd(để trình duyệt hiểu)
  function convertDateFormat(paramDateString) {
    if (paramDateString) {
      const dateParts = paramDateString.split("-");
      return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }
  }

  // Hàm xử lý sự kiện hidden modal create
  function clearFormModalCreate() {
    $("#input-size-create").val("");
    $("#input-diameter-create").val("");
    $("#input-ribs-create").val("");
    $("#input-salad-create").val("");
    $("#input-drink-create").val("");
    $("#input-price-create").val("");
  }

  // Hàm xử lý sự kiện hidden modal update
  function clearFormModalUpdate() {
    $("#input-size-update").val("");
    $("#input-diameter-update").val("");
    $("#input-ribs-update").val("");
    $("#input-salad-update").val("");
    $("#input-drink-update").val("");
    $("#input-price-update").val("");
    $("#input-created-at-update").val("");
    $("#input-updated-at-update").val("");
  }

  // Hàm hiển thị thông báo
  function showToast(paramType, paramMessage) {
    switch (paramType) {
      case 1: //success
        toastr.success(paramMessage);
        break;
      case 2: //info
        toastr.info(paramMessage);
        break;
      case 3: //error
        toastr.error(paramMessage);
        break;
      case 4: //warning
        toastr.warning(paramMessage);
        break;
    }
  }
});
