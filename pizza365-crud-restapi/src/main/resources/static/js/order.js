$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_ORDERS = "http://localhost:8080/orders";
  var URL_API_USERS = "http://localhost:8080/users";
  var gOrderId;
  var gUsers;
  var gUserId;

  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gCOLS = [
    "id",
    "orderCode",
    "pizzaSize",
    "pizzaType",
    "voucherCode",
    "price",
    "paid",
    "userFullName",
    "createdAt",
    "updatedAt",
    "action",
  ];

  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT_COL = 0;
  const gORDER_CODE_COL = 1;
  const gPIZZA_SIZE_COL = 2;
  const gPIZZA_TYPE_COL = 3;
  const gVOUCHER_CODE_COL = 4;
  const gPRICE_COL = 5;
  const gPAID_COL = 6;
  const gUSER_NAME_COL = 7;
  const gCREATE_DAY_COL = 8;
  const gUPDATE_DAY_COL = 9;
  const gACTION_COL = 10;

  //định nghĩa table
  var gTable = $("#order-table").DataTable({
    columns: [
      { data: gCOLS[gSTT_COL] },
      { data: gCOLS[gORDER_CODE_COL] },
      { data: gCOLS[gPIZZA_SIZE_COL] },
      { data: gCOLS[gPIZZA_TYPE_COL] },
      { data: gCOLS[gVOUCHER_CODE_COL] },
      { data: gCOLS[gPRICE_COL] },
      { data: gCOLS[gPAID_COL] },
      { data: gCOLS[gUSER_NAME_COL] },
      { data: gCOLS[gCREATE_DAY_COL] },
      { data: gCOLS[gUPDATE_DAY_COL] },
      { data: gCOLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        target: gSTT_COL,
        className: "text-center",
        render: (data, type, row, meta) => {
          return meta.row + 1;
        },
      },
      {
        target: gPRICE_COL,
        render: (data, type, row, meta) => {
          return data + "$";
        },
      },
      {
        target: gPAID_COL,
        render: (data, type, row, meta) => {
          return data + "$";
        },
      },
      {
        target: gACTION_COL,
        className: "text-center",
        defaultContent: `
          <div class="d-flex justify-content-around">
            <i class="fas fa-edit update-icon btn text-primary mx-auto"></i>
            <i class="fas fa-trash-alt delete-icon btn text-danger mx-auto"></i>
          </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //Event show modal
  $("#create-modal, #update-modal").on("shown.bs.modal", function () {
    //load select user
    loadDataToUserSelect(gUsers);
    // Kiểm tra xem modal hiện tại có id là "update-modal" hay không
    if ($(this).attr("id") === "update-modal") {
      //gán user vào select update
      $("#select-user-update").val(gUserId);
    }
  });

  //Event click btn thêm mới
  $("#btn-create").click(() => {
    $("#create-modal").modal("show");
  });

  //Event click icon update
  $("#order-table").on("click", ".update-icon", function () {
    onIconUpdateClick(this);
  });

  //Event click icon delete
  $("#order-table").on("click", ".delete-icon", function () {
    onIconDeleteClick(this);
  });

  //Event click btn confirm create
  $("#btn-confirm-create").click(onBtnConfirmCreateClick);

  //Event click btn confirm update
  $("#btn-confirm-update").click(onBtnConfirmUpdateClick);

  //Event click btn confirm delete
  $("#btn-confirm-delete").click(callAPIDeleteOrderById);

  //Event hidden modal create
  $("#create-modal").on("hidden.bs.modal", clearFormModalCreate);

  //Event hidden modal update
  $("#update-modal").on("hidden.bs.modal", clearFormModalUpdate);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    callAPIGetAllUsers();
    callAPIGetAllOrders();
  }

  // Hàm xử lý sự kiện click icon update
  function onIconUpdateClick(paramUpdateIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramUpdateIcon);
    //B3: call API get by ID
    callAPIGetOrderById();
  }

  // Hàm xử lý sự kiện click icon delete
  function onIconDeleteClick(paramDeleteIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramDeleteIcon);
    $("#delete-modal").modal("show");
  }

  // Hàm xử lý sự kiện click nút confirm tạo mới
  function onBtnConfirmCreateClick() {
    // đối tượng sẽ được tạo mới
    var orderObj = {
      orderCode: "",
      pizzaSize: "",
      pizzaType: "",
      voucherCode: "",
      price: -1,
      paid: -1,
      user: {
        id: "",
      },
    };
    // B1: Thu thập dữ liệu
    collectDataCreate(orderObj);
    // B2: Validate
    let vCheck = validationData(orderObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPICreateOrder(orderObj);
    }
  }

  // Hàm xử lý sự kiện click nút confirm update
  function onBtnConfirmUpdateClick() {
    // đối tượng sẽ được tạo mới
    var orderObj = {
      orderCode: "",
      pizzaSize: "",
      pizzaType: "",
      voucherCode: "",
      price: -1,
      paid: -1,
      user: {
        id: "",
      },
    };
    // B1: Thu thập dữ liệu
    collectDataUpdate(orderObj);
    // B2: Validate
    let vCheck = validationData(orderObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPIUpdateOrderById(orderObj);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllOrders() {
    $.ajax({
      url: URL_API_ORDERS,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToTable(res);
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIGetAllUsers() {
    $.ajax({
      url: URL_API_USERS,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUserSelect(res);
        gUsers = res;
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIGetOrderById() {
    $.ajax({
      url: URL_API_ORDERS + "/" + gOrderId,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUpdateForm(res);
        $("#update-modal").modal("show");
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPICreateOrder(paramOrder) {
    $.ajax({
      url: URL_API_ORDERS,
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramOrder),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Tạo mới thành công");
        $("#create-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Tạo mới thất bại");
      },
    });
  }

  function callAPIUpdateOrderById(paramOrder) {
    $.ajax({
      url: URL_API_ORDERS + "/" + gOrderId,
      method: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramOrder),
      dataType: "json",
      success: function (res) {
        console.log(res);
        showToast(1, "Cập nhật thành công");
        $("#update-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Cập nhật thất bại");
      },
    });
  }

  function callAPIDeleteOrderById() {
    $.ajax({
      url: URL_API_ORDERS + "/" + gOrderId,
      method: "DELETE",
      success: function (res) {
        console.log(res);
        showToast(1, "Xóa thành công");
        $("#delete-modal").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        showToast(3, "Xóa thất bại");
      },
    });
  }

  //handle function
  function loadDataToTable(paramResponse) {
    gTable.clear();
    gTable.rows.add(paramResponse);
    gTable.draw();
  }

  function loadDataToUpdateForm(paramResponse) {
    gUserId = paramResponse.userId;
    gOrderId = paramResponse.id;
    $("#input-code-update").val(paramResponse.orderCode);
    $("#select-size-update").val(paramResponse.pizzaSize);
    $("#select-type-update").val(paramResponse.pizzaType);
    $("#input-voucher-update").val(paramResponse.voucherCode);
    $("#input-price-update").val(paramResponse.price);
    $("#input-paid-update").val(paramResponse.paid);
    let getNgayTao = new Date(convertDateFormat(paramResponse.createdAt));
    $("#input-created-at-update").val(getNgayTao.toLocaleDateString("en-CA")); //en-CA có dạng yyyy-MM-dd
    let getNgayCapNhat = new Date(convertDateFormat(paramResponse.updatedAt));
    $("#input-updated-at-update").val(
      getNgayCapNhat.toLocaleDateString("en-CA")
    );
    //form select user sẽ gán ở sự kiện show modal update
  }

  function loadDataToUserSelect(paramUsers) {
    $(".user-select").html("");
    for (let i = 0; i < paramUsers.length; i++) {
      let option = $("<option/>");
      option.prop("value", paramUsers[i].id);
      option.prop("text", paramUsers[i].fullName);
      $(".user-select").append(option);
    }
  }

  // collect function
  function collectIdRowClick(paramIcon) {
    let vRowClick = $(paramIcon).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gOrderId = vRowData.id; //lưu id
  }

  function collectDataCreate(paramOrder) {
    paramOrder.orderCode = $("#input-code-create").val();
    paramOrder.pizzaSize = $("#select-size-create").val();
    paramOrder.pizzaType = $("#select-type-create").val();
    paramOrder.voucherCode = $("#input-voucher-create").val();
    paramOrder.price = parseInt($("#input-price-create").val());
    paramOrder.paid = parseInt($("#input-paid-create").val());
    paramOrder.user.id = $("#select-user-create").val();
  }

  function collectDataUpdate(paramOrder) {
    paramOrder.orderCode = $("#input-code-update").val();
    paramOrder.pizzaSize = $("#select-size-update").val();
    paramOrder.pizzaType = $("#select-type-update").val();
    paramOrder.voucherCode = $("#input-voucher-update").val();
    paramOrder.price = parseInt($("#input-price-update").val());
    paramOrder.paid = parseInt($("#input-paid-update").val());
    paramOrder.user.id = $("#select-user-update").val();
  }

  function validationData(paramOrder) {
    if (paramOrder.orderCode === "") {
      showToast(3, "Vui lòng nhập mã đơn hàng");
      return false;
    }
    if (paramOrder.pizzaSize === "") {
      showToast(3, "Vui lòng nhập pizza size");
      return false;
    }
    if (paramOrder.pizzaType === "") {
      showToast(3, "Vui lòng nhập pizza type");
      return false;
    }
    if (paramOrder.price < 0 || isNaN(paramOrder.price)) {
      showToast(3, "Tổng tiền phải là số >= 0");
      return false;
    }
    if (paramOrder.paid < 0 || isNaN(paramOrder.paid)) {
      showToast(3, "Số tiền đã thanh toán phải là số >= 0");
      return false;
    }
    if (paramOrder.user.id === "") {
      showToast(3, "Vui lòng chọn user");
      return false;
    }
    return true;
  }

  //chuyển ngày dd-MM-yyyy thành yyyy-MM-dd(để trình duyệt hiểu)
  function convertDateFormat(paramDateString) {
    if (paramDateString) {
      const dateParts = paramDateString.split("-");
      return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }
  }

  // Hàm xử lý sự kiện hidden modal create
  function clearFormModalCreate() {
    $("#input-code-create").val("");
    $("#select-size-create").val("S");
    $("#select-type-create").val("Bacon");
    $("#input-voucher-create").val("");
    $("#input-price-create").val("");
    $("#input-paid-create").val("");
    $("#select-user-create").val("");
  }

  // Hàm xử lý sự kiện hidden modal update
  function clearFormModalUpdate() {
    $("#input-code-update").val("");
    $("#select-size-update").val("S");
    $("#select-type-update").val("Bacon");
    $("#input-voucher-update").val("");
    $("#input-price-update").val("");
    $("#input-paid-update").val("");
    $("#select-user-update").val("");
    $("#input-created-at-update").val("");
    $("#input-updated-at-update").val("");
  }

  // Hàm hiển thị thông báo
  function showToast(paramType, paramMessage) {
    switch (paramType) {
      case 1: //success
        toastr.success(paramMessage);
        break;
      case 2: //info
        toastr.info(paramMessage);
        break;
      case 3: //error
        toastr.error(paramMessage);
        break;
      case 4: //warning
        toastr.warning(paramMessage);
        break;
    }
  }
});
