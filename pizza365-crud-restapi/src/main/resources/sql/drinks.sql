-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 09, 2024 lúc 11:57 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `pizza_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `drinks`
--

-- CREATE TABLE `drinks` (
--   `id` bigint(20) NOT NULL,
--   `ma_nuoc_uong` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `ngay_cap_nhat` bigint(20) DEFAULT NULL,
--   `ngay_tao` bigint(20) DEFAULT NULL,
--   `gia_nuoc_uong` bigint(20) DEFAULT NULL,
--   `ten_nuoc_uong` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `ghi_chu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `drinks`
--

INSERT INTO `drinks` (`id`, `ma_nuoc_uong`, `ngay_cap_nhat`, `ngay_tao`, `gia_nuoc_uong`, `ten_nuoc_uong`, `ghi_chu`) VALUES
(1, 'TRA_TAC', 1706841600, 1706841600, 15000, 'Trà Tắc', NULL),
(2, 'COCA', 1706841600, 1706841600, 10000, 'Cocacola', NULL),
(3, 'PEPSI', 1706841600, 1706841600, 10000, 'Pepsi', NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
