-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2024 at 04:48 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

-- CREATE TABLE `orders` (
--   `id` bigint(20) NOT NULL,
--   `created_at` datetime DEFAULT NULL,
--   `order_code` varchar(20) NOT NULL,
--   `paid` bigint(20) NOT NULL,
--   `pizza_size` varchar(5) NOT NULL,
--   `pizza_type` varchar(20) DEFAULT NULL,
--   `price` bigint(20) NOT NULL,
--   `updated_at` datetime DEFAULT NULL,
--   `voucher_code` varchar(255) DEFAULT NULL,
--   `user_id` bigint(20) DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `order_code`, `paid`, `pizza_size`, `pizza_type`, `price`, `updated_at`, `voucher_code`, `user_id`) VALUES
(1, '2024-05-08 21:46:19', 'ORDER001', 200000, 'L', 'Hawaiian', 250000, '2024-05-08 21:46:19', 'VOUCHER123', 1),
(2, '2024-05-08 21:46:19', 'ORDER002', 180000, 'M', 'Pepperoni', 180000, '2024-05-08 21:46:19', NULL, 1),
(3, '2024-05-08 21:46:19', 'ORDER003', 300000, 'XL', 'Margherita', 300000, '2024-05-08 21:46:19', NULL, 2),
(4, '2024-05-08 21:46:19', 'ORDER004', 150000, 'S', 'Vegetarian', 150000, '2024-05-08 21:46:19', 'DISCOUNT456', 2),
(5, '2024-05-08 21:46:19', 'ORDER005', 220000, 'L', 'BBQ Chicken', 220000, '2024-05-08 21:46:19', NULL, 3),
(6, '2024-05-08 21:46:19', 'ORDER006', 180000, 'M', 'Seafood Deluxe', 190000, '2024-05-08 21:46:19', 'SUMMER123', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_dhk2umg8ijjkg4njg6891trit` (`order_code`),
  ADD KEY `FK32ql8ubntj5uh44ph9659tiih` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK32ql8ubntj5uh44ph9659tiih` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
