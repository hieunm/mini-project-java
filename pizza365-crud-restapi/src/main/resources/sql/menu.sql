-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2024 at 05:09 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

-- CREATE TABLE `menu` (
--   `id` bigint(20) NOT NULL,
--   `duong_kinh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `kick_co` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `nuoc_ngot` int(11) DEFAULT NULL,
--   `salad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
--   `suon_nuong` int(11) DEFAULT NULL,
--   `thanh_tien` int(11) DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `duong_kinh`, `kick_co`, `nuoc_ngot`, `salad`, `suon_nuong`, `thanh_tien`) VALUES
(1, '20 cm', 'S', 2, '200 g', 2, 150000),
(2, '25 cm', 'M', 3, '300 g', 4, 200000),
(3, '30 cm', 'L', 4, '500 g', 8, 250000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
