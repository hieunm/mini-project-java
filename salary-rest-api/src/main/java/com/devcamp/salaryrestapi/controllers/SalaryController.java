package com.devcamp.salaryrestapi.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.salaryrestapi.dtos.SalaryDTO;
import com.devcamp.salaryrestapi.services.SalaryService;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;




@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class SalaryController {
    @Autowired
    private SalaryService salaryService;

    @PostMapping("/gross-to-net")
    public ResponseEntity<Object> calculatorSalaryGrossToNet(@Valid @RequestBody SalaryDTO pSalary) {
        try {
            SalaryDTO salaryDTO = salaryService.calculatorSalaryGrossToNet(pSalary);
            return new ResponseEntity<>(salaryDTO, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    
}
