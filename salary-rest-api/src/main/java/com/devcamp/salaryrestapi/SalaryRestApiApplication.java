package com.devcamp.salaryrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalaryRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalaryRestApiApplication.class, args);
	}

}
