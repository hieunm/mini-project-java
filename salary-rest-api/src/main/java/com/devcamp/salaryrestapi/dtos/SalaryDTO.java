package com.devcamp.salaryrestapi.dtos;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

public class SalaryDTO {
    @NotNull(message = "Nhập lương gross")
    @Range(min = 0, message = "Lương gross phải >= 0")
    private double grossSalary;

    @NotNull(message = "Nhập lương đóng bảo hiểm")
    @Range(min = 0, message = "Lương đóng bảo hiểm phải >= 0")
    private double insuranceSalary;

    @NotNull(message = "Nhập người phụ thuộc")
    @Range(min = 0, max = 20, message = "Người phụ thuộc phải từ 0-20")
    private int dependents;

    @NotNull(message = "Nhập vùng")
    @Range(min = 1, max = 4, message = "Vùng phải từ 1-4")
    private int region;

    private double netSalary;

    public SalaryDTO() {
    }

    public SalaryDTO(double grossSalary, double insuranceSalary, int dependents, int region, double netSalary) {
        this.grossSalary = grossSalary;
        this.insuranceSalary = insuranceSalary;
        this.dependents = dependents;
        this.region = region;
        this.netSalary = netSalary;
    }

    public double getGrossSalary() {
        return grossSalary;
    }

    public void setGrossSalary(double grossSalary) {
        this.grossSalary = grossSalary;
    }

    public double getInsuranceSalary() {
        return insuranceSalary;
    }

    public void setInsuranceSalary(double insuranceSalary) {
        this.insuranceSalary = insuranceSalary;
    }

    public int getDependents() {
        return dependents;
    }

    public void setDependents(int dependents) {
        this.dependents = dependents;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public double getNetSalary() {
        return netSalary;
    }

    public void setNetSalary(double netSalary) {
        this.netSalary = netSalary;
    }

}
