package com.devcamp.salaryrestapi.constants;

public class SalaryConstants {
    public static final double BH_XH = 0.08;
    public static final double BH_YT = 0.015;
    public static final double BH_TN = 0.01;
    public static final double BH_TOTAL = BH_XH + BH_YT + BH_TN;

    public static final double SELF_DEDUCT = -11000000;
    public static final double DEPENDENT_DEDUCT = -4400000;
}
