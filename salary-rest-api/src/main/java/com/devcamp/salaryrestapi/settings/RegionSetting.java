package com.devcamp.salaryrestapi.settings;

import java.util.Arrays;

public enum RegionSetting {
    REGION1(1, 4680000),
    REGION2(2, 4160000),
    REGION3(3, 3640000),
    REGION4(4, 3250000);

    private final int regionCode;
    private final double regionMinSalary;

    private RegionSetting(int regionCode, double regionMinSalary) {
        this.regionCode = regionCode;
        this.regionMinSalary = regionMinSalary;
    }

    public int getRegionCode() {
        return regionCode;
    }

    public double getRegionMinSalary() {
        return regionMinSalary;
    }

    public static void checkRegionMinsalary(int region, double grossSalary) {
        RegionSetting regionSetting = Arrays.stream(RegionSetting.values())
                .filter(regionInfo -> regionInfo.getRegionCode() == region)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Mã vùng " + region + " không hợp lệ"));
        double minSalary = regionSetting.getRegionMinSalary();
        if (grossSalary < minSalary) {
            throw new IllegalArgumentException(String.format("Lương tối thiểu vùng %d là %f/tháng", region, minSalary));
        }
    }
    

}
