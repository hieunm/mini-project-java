package com.devcamp.salaryrestapi.services;

import org.springframework.stereotype.Service;

import com.devcamp.salaryrestapi.calculator.TaxCalculator;
import com.devcamp.salaryrestapi.constants.SalaryConstants;
import com.devcamp.salaryrestapi.dtos.SalaryDTO;
import com.devcamp.salaryrestapi.settings.RegionSetting;

@Service
public class SalaryService {
    public SalaryDTO calculatorSalaryGrossToNet(SalaryDTO pSalary) {
        //check lương tối thiểu
        RegionSetting.checkRegionMinsalary(pSalary.getRegion(), pSalary.getGrossSalary());
        //Tính thu nhập trc thuế
        double preTaxIncome = pSalary.getGrossSalary() - pSalary.getInsuranceSalary() * SalaryConstants.BH_TOTAL;
        //Tính thu nhập chịu thuế
        double taxableIncome = preTaxIncome - SalaryConstants.SELF_DEDUCT - SalaryConstants.DEPENDENT_DEDUCT * pSalary.getDependents();
        //Tính thuế thu nhập cá nhân
        double personalIncomeTax = taxableIncome * TaxCalculator.calculateTax(taxableIncome);
        //Tính lương net
        double netSalary = preTaxIncome - personalIncomeTax;
        pSalary.setNetSalary(Math.round(netSalary * 100.0) / 100.0);
        return pSalary;
    }
}
