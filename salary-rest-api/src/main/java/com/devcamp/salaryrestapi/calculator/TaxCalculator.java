package com.devcamp.salaryrestapi.calculator;

public class TaxCalculator {
    public static double calculateTax(double taxableIncome) {
        if (taxableIncome < 0) {
            throw new IllegalArgumentException("Thu nhập chịu thuế không thể nhỏ hơn 0");
        }
        double taxRate;
        if (taxableIncome <= 5000000) {
            taxRate = 0.05;
        } else if (taxableIncome <= 10000000) {
            taxRate = 0.1;
        } else if (taxableIncome <= 18000000) {
            taxRate = 0.15;
        } else if (taxableIncome <= 32000000) {
            taxRate = 0.2;
        } else if (taxableIncome <= 52000000) {
            taxRate = 0.25;
        } else if (taxableIncome <= 80000000) {
            taxRate = 0.3;
        } else {
            taxRate = 0.35;
        }
    
        return taxRate;
    }
}
