//Lắng nghe sự kiện click nút submit
$("#btn-submit").click(onBtnSubmitClick);

function onBtnSubmitClick() {
    event.preventDefault();
    let salaryObj = {
        grossSalary: 0,
        insuranceSalary: 0,
        dependents: 0,
        region: 1
      }
    //B1: Thu thập dl
    collectData(salaryObj)
    //B2: Validate(Bỏ qua cho server tự check)
    //B3: callapi
    callAPICalculateSalary(salaryObj);
}

function collectData(paramSalaryObj) {
    paramSalaryObj.grossSalary = parseInt($('#input-gross-salary').val().trim());
    paramSalaryObj.insuranceSalary = parseInt($('#input-insurance-salary').val().trim());
    paramSalaryObj.dependents = parseInt($('#input-dependent').val().trim());
    paramSalaryObj.region = parseInt($('#select-region').val());
}

function callAPICalculateSalary(paramSalaryObj) {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    const raw = JSON.stringify(paramSalaryObj);
    
    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow"
    };
    
    fetch("http://localhost:8080/gross-to-net", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        $("#result-container").html(`Mức lương NET của bạn là: ${result.netSalary} VND`).removeClass('d-none');
      })
      .catch((error) => console.error(error));
}

