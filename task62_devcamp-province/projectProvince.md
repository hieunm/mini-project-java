# Dự án Quản lý các đơn vị hành chính

## 📄 Giới thiệu
> ### 1. Tổng quan
- Đây là một hệ thống quản lý các đơn vị hành chính bằng phần mềm. Hệ thống này được xây dựng với mục tiêu đơn giản hóa việc quản lý các đơn vị hành chính bằng phần mềm và hướng tới việc giúp công dân có thể dễ dàng tiếp cận được thông tin. Ngoài ra cũng giúp cho các cơ quan ban ngành có thể dễ dàng quản lý được các đơn vị hành chính lớn nhỏ.
- Dự án được xây dựng trên nền tảng Spring Boot và Spring Framework để cung cấp các endpoint RESTful API cho việc truy cập dữ liệu.

> ### 2. Kiến trúc Hệ thống
Hệ thống bao gồm hai phần chính:

> **_Frontend_**
- Phần giao diện người dùng của hệ thống, cho phép người dùng thực hiện các hoạt động như xem danh sách các tỉnh, quận huyện tương ứng với tỉnh đó và các phường xã trực thuộc huyện được chọn.
- Ngoài ra cho phép CRUD danh sách các tỉnh, quận huyện, xã phường.

> **_Backend_**
- Dự án sử dụng mô hình MVC (Model-View-Controller) và quan hệ OneToMany để tổ chức cấu trúc và logic của ứng dụng. Các endpoint được điều hướng từ controller tới các service tương ứng để xử lý.
- Phần xử lý logic và dữ liệu của hệ thống, bao gồm các service REST API sau:
- READ: Cung cấp thông tin về danh sách các đơn vị hành chính, truy vấn các đơn vị hành chính theo Id, truy vấn danh sách các đơn vị hành chính trực thuộc 1 đơn vị hành chính lớn hơn dựa vào Id và trả về danh sách các tỉnh theo thông tin phân trang truyền vào.
- CREATE: Thêm mới các đơn vị hành chính.
- UPDATE: Cập nhật thông tin các đơn vị hành chính. Tự động tải dữ liệu tương ứng khi bấm chọn 1 đơn vị hành chính bất kì.
- DELETE: Xóa thông tin các đơn vị hành chính.

## ✨ Chức Năng
> ### 1. Frontend
- Trang chủ
![Home](src/main/resources/static/images/home.PNG)
- Tự động tải danh sách các tỉnh khi tải trang
![Home](src/main/resources/static/images/home_province.png)
- Tự động tải danh sách các quận huyện tương ứng khi chọn tỉnh
![Home](src/main/resources/static/images/home_district.png)
- Tự động tải danh sách các xã phường tương ứng khi chọn quận huyện
![Home](src/main/resources/static/images/home_ward.png)
- Trang admin quản lý CRUD các tỉnh thành phố
![Province](src/main/resources/static/images/province.PNG)
- Trang admin quản lý CRUD các quận huyện
![District](src/main/resources/static/images/district.PNG)
- Trang admin quản lý CRUD các xã phường
![Ward](src/main/resources/static/images/ward.PNG)
- Form tạo mới(tự động đổ dữ liệu vào form)
![Create](src/main/resources/static/images/create.PNG)
- Form cập nhật(tự động đổ dữ liệu vào form)
![Update](src/main/resources/static/images/update.PNG)
- Form xóa
![Delete](src/main/resources/static/images/delete.PNG)
- Validation dữ liệu trước khi CRUD
![Validation](src/main/resources/static/images/validation.PNG)

> ### 2. Backend
> **_Get All_**
- Endpoint: ***"http://localhost:8080/" + "provinces"/"districts"/"wards"***
- Phương thức: GET
- Mô tả: Endpoint này cung cấp thông tin danh sách toàn bộ các đơn vị hành chính được sắp xếp theo 1 thứ tự do ta quy định.

> **_Get By Id_**
- Endpoint: ***"http://localhost:8080/" + "provinces"/"districts"/"wards" + id***
- Phương thức: GET
- Mô tả: Endpoint này cung cấp thông tin 1 đơn vị hành chính theo id của nó.

> **_Get ? By ? Id_**
- Endpoint: ***"http://localhost:8080/province/districts?provinceId=?" && "http://localhost:8080/district/wards?wardId=?"***
- Phương thức: GET
- Mô tả: Endpoint này cung cấp thông tin danh sách toàn bộ các đơn vị hành chính trực thuộc 1 đơn vị hành chính lớn hơn.

> **_Get Province Panigation_**
- Endpoint: ***"http://localhost:8080/provinces/pagination?page=?&size=?"***
- Phương thức: GET
- Mô tả: Endpoint này cung cấp thông tin danh sách các tỉnh theo thông số phân trang truyền vào.

> **_Create_**
- Endpoint: ***"http://localhost:8080/" + "provinces"/"districts"/"wards"***
- Phương thức: POST
- Mô tả: Endpoint này tạo mới 1 đơn vị hành chính.

> **_Update By Id_**
- Endpoint: ***"http://localhost:8080/" + "provinces"/"districts"/"wards" + id***
- Phương thức: PUT
- Mô tả: Endpoint này cập nhật thông tin 1 đơn vị hành chính theo id của nó.

> **_Delete By Id_**
- Endpoint: ***"http://localhost:8080/" + "provinces"/"districts"/"wards" + id***
- Phương thức: DELETE
- Mô tả: Endpoint này xóa 1 đơn vị hành chính theo id của nó.

> ### 3. Cách Sử Dụng
- Gửi các yêu cầu HTTP tới các endpoint được cung cấp để truy cập thông tin bạn cần.
- Sử dụng thư viện HTTP client phù hợp trong ngôn ngữ lập trình của bạn để tương tác với REST API.

## 🌟 Giải thích Code
> ### 1. getAll...(): 
- Sử dụng phương thức findAll() của interface JpaRepository để get toàn bộ danh sách.
- Sử dụng ResponseEntity để trả về cho client, sử dụng try_catch để bắt ngoại lệ. Kiểm tra thông tin lấy được và xử lý phản hồi tương ứng cho client.
> ### 2. get...By...Id()
- Sử dụng phương thức findById() của interface JpaRepository để get bản ghi theo Id.
- findById() sẽ trả về 1 Optional, kiểm tra xem Optional có chứa dữ liệu ko để xử lý trả về(lấy thông tin cần thiết hoặc return null).
- Sử dụng ResponseEntity để trả về cho client, sử dụng try_catch để bắt ngoại lệ. Kiểm tra thông tin lấy được và xử lý phản hồi tương ứng cho client.
> ### 3. getProvincesPanigation()
- Sử dụng class PageRequest để tìm kiếm dữ liệu theo thông tin phân trang truyền vào.
- Phương thức sẽ trả về 1 đối tượng Pageable. Lấy dữ liệu của đối tượng để trả về cho controller xử lý.
- Sử dụng ResponseEntity để trả về cho client, sử dụng try_catch để bắt ngoại lệ. Kiểm tra thông tin lấy được và xử lý phản hồi tương ứng cho client.
> ### 4. create...()
- Thực hiện các kiểm tra logic(validation) lên đối tượng được truyền vào trong body.
- Sử dụng phương thức save() của interface JpaRepository để lưu đối tượng mới vào db.
- Sử dụng ResponseEntity để trả về cho client, sử dụng try_catch để bắt ngoại lệ. Kiểm tra thông tin lấy được và xử lý phản hồi tương ứng cho client.
> ### 5. update...ById()
- Sử dụng phương thức findById() của interface JpaRepository để get bản ghi theo Id.
- Thực hiện các kiểm tra logic(validation) lên đối tượng được truyền vào trong body.
- Sử dụng phương thức save() của interface JpaRepository để cập nhật đối tượng vào db.
- Sử dụng ResponseEntity để trả về cho client, sử dụng try_catch để bắt ngoại lệ. Kiểm tra thông tin lấy được và xử lý phản hồi tương ứng cho client.
> ### 6. delete...ById()
- Sử dụng phương thức findById() của interface JpaRepository để get bản ghi theo Id.
- Sử dụng phương thức deleteById() của interface JpaRepository để xóa bản ghi theo Id.
- Sử dụng ResponseEntity để trả về cho client, sử dụng try_catch để bắt ngoại lệ. Kiểm tra thông tin lấy được và xử lý phản hồi tương ứng cho client.

## 🧱 Technology

> ### Front-end:
> 1. [HTML5](https://developer.mozilla.org/en-US/docs/Web/HTML)
> 2. [CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS)
> 3. [JavaScript ES6](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
> 4. [Bootstrap 4](https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css)
> 5. [Jquery 3](https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js)
> 6. [AdminLTE 3](https://github.com/ColorlibHQ/AdminLTE/releases)
> 7. Ajax
> 8. JSON

> ### Back-end:
> 1. [Java (17)](https://www.java.com/en/)
> 2. [Spring Boot (v2.6.7)](https://spring.io/projects/spring-boot)
> 3. [Spring Framework)](https://spring.io/projects/spring-framework)

> ### Database:
> 1. [MySQL5](https://www.mysql.com/)
