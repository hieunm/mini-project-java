$(document).ready(function () {
    "use strict"
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_GET_PROVINCES = "http://localhost:8080/provinces";
  var URL_API_DISTRICTS_BY_PROVINCE_ID =
    "http://localhost:8080/province/districts?provinceId=";
  var URL_API_WARDS_BY_DISTRICT_ID =
    "http://localhost:8080/district/wards?districtId=";

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //sự kiện thay đổi select province
  $("#province-select").on("change", function () {
    //xóa trắng select district+ward để tránh ng dùng bị nhầm lẫn dữ liệu trong khi chờ callAPI
    $("#district-select").html("").prop("disabled", true);
    $("#ward-select").html("").prop("disabled", true);
    //Lấy provinceId tương ứng province đang đc lựa chọn
    let provinceId = $(this).val();
    //call API
    callAPIGetDistrictsByProvinceId(provinceId);
  });

  //sự kiện thay đổi select district
  $("#district-select").on("change", function () {
    //xóa trắng select ward để tránh ng dùng bị nhầm lẫn dữ liệu trong khi chờ callAPI
    $("#ward-select").html("").prop("disabled", true);
    //Lấy districtId tương ứng district đang đc lựa chọn
    let districtId = $(this).val();
    //call API
    callAPIGetWardsByDistrictId(districtId);
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  async function onPageLoading() {
    try {
      await callAPIGetAllProvinces();
      const provinceId = $("#province-select").val();
      await callAPIGetDistrictsByProvinceId(provinceId);
      const districtId = $("#district-select").val();
      await callAPIGetWardsByDistrictId(districtId);
    } catch (error) {
      console.error("Error:", error);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllProvinces() {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: URL_API_GET_PROVINCES,
        method: "GET",
        success: function (res) {
          console.log(res);
          loadDataToProvinceSelect(res);
          resolve(res);
        },
        error: function (err) {
          console.error(err);
          reject(err);
        },
      });
    });
  }

  function callAPIGetDistrictsByProvinceId(provinceId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: URL_API_DISTRICTS_BY_PROVINCE_ID + provinceId,
        method: "GET",
        success: function (res) {
          console.log(res);
          loadDataToDistrictSelect(res);
          resolve(res);
        },
        error: function (err) {
          console.error(err);
          reject(err);
        },
      });
    });
  }

  function callAPIGetWardsByDistrictId(districtId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: URL_API_WARDS_BY_DISTRICT_ID + districtId,
        method: "GET",
        success: function (res) {
          console.log(res);
          loadDataToWardSelect(res);
          resolve(res);
        },
        error: function (err) {
          console.error(err);
          reject(err);
        },
      });
    });
  }

  //Load data to select
  function loadDataToProvinceSelect(provinces) {
    for (let i = 0; i < provinces.length; i++) {
      let option = $("<option/>");
      option.prop("value", provinces[i].id);
      option.prop("text", provinces[i].name);
      $("#province-select").append(option);
    }
  }

  function loadDataToDistrictSelect(districts) {
    if (districts.length > 0) {
      $("#district-select").prop("disabled", false); //bật select

      for (let i = 0; i < districts.length; i++) {
        let option = $("<option/>");
        option.prop("value", districts[i].id);
        option.prop("text", districts[i].name);
        $("#district-select").append(option);
      }
    }
  }

  function loadDataToWardSelect(wards) {
    if (wards.length > 0) {
      $("#ward-select").prop("disabled", false); //bật select

      for (let i = 0; i < wards.length; i++) {
        let option = $("<option/>");
        option.prop("value", wards[i].id);
        option.prop("text", wards[i].name);
        $("#ward-select").append(option);
      }
    }
  }
});
