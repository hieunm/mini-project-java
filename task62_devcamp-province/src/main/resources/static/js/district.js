$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_DISTRICTS = "http://localhost:8080/districts";
  var URL_API_PROVINCES = "http://localhost:8080/provinces";
  var gId;
  var gProvinces;

  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gCOLS = [
    "id",
    "name",
    "prefix",
    "provinceName",
    "createdAt",
    "updatedAt",
    "action",
  ];

  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT_COL = 0;
  const gNAME_COL = 1;
  const gPREFIX_COL = 2;
  const gPROVINCE_NAME_COL = 3;
  const gCREATE_DAY_COL = 4;
  const gUPDATE_DAY_COL = 5;
  const gACTION_COL = 6;

  //định nghĩa table
  var gTable = $("#district-table").DataTable({
    columns: [
      { data: gCOLS[gSTT_COL] },
      { data: gCOLS[gNAME_COL] },
      { data: gCOLS[gPREFIX_COL] },
      { data: gCOLS[gPROVINCE_NAME_COL] },
      { data: gCOLS[gCREATE_DAY_COL] },
      { data: gCOLS[gUPDATE_DAY_COL] },
      { data: gCOLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        target: gSTT_COL,
        className: "text-center",
        render: (data, type, row, meta) => {
          return meta.row + 1;
        },
      },
      {
        target: gACTION_COL,
        className: "text-center",
        defaultContent: `
          <div class="d-flex justify-content-around">
            <i class="fas fa-edit update-icon btn text-primary mx-auto"></i>
            <i class="fas fa-trash-alt delete-icon btn text-danger mx-auto"></i>
          </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //Event show modal
  $('#create-modal, #update-modal').on('shown.bs.modal', function (e) {
    loadDataToProvinceSelect(gProvinces);
  });

  //Event click btn thêm mới
  $("#btn-create").click(() => {
    $("#create-modal").modal("show");
  });

  //Event click icon update
  $("#district-table").on("click", ".update-icon", function () {
    onIconUpdateClick(this);
  });

  //Event click icon delete
  $("#district-table").on("click", ".delete-icon", function () {
    onIconDeleteClick(this);
  });

  //Event click btn confirm create
  $("#btn-confirm-create").click(onBtnConfirmCreateClick);

  //Event click btn confirm update
  $("#btn-confirm-update").click(onBtnConfirmUpdateClick);

  //Event click btn confirm delete
  $("#btn-confirm-delete").click(callAPIDeleteDistrictById);

  //Event hidden modal create
  $("#create-modal").on("hidden.bs.modal", clearFormModalCreate);

  //Event hidden modal update
  $("#update-modal").on("hidden.bs.modal", clearFormModalUpdate);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    callAPIGetAllDistricts();
    callAPIGetAllProvinces();
  }

  // Hàm xử lý sự kiện click icon update
  function onIconUpdateClick(paramUpdateIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramUpdateIcon);
    //B3: call API get by ID
    callAPIGetDistrictById();
  }

  // Hàm xử lý sự kiện click icon delete
  function onIconDeleteClick(paramDeleteIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramDeleteIcon);
    $("#delete-modal").modal("show");
  }

  // Hàm xử lý sự kiện click nút confirm tạo mới
  function onBtnConfirmCreateClick() {
    // đối tượng sẽ được tạo mới
    var districtObj = {
      name: "",
      prefix: "",
      province: {
        id: "",
      },
    };
    // B1: Thu thập dữ liệu
    collectDataCreate(districtObj);
    // B2: Validate
    let vCheck = validationData(districtObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPICreateDistrict(districtObj);
    }
  }

  // Hàm xử lý sự kiện click nút confirm update
  function onBtnConfirmUpdateClick() {
    // đối tượng sẽ được tạo mới
    var districtObj = {
      name: "",
      prefix: "",
      province: {
        id: "",
      }
    };
    // B1: Thu thập dữ liệu
    collectDataUpdate(districtObj);
    // B2: Validate
    let vCheck = validationData(districtObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPIUpdateDistrictById(districtObj);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllDistricts() {
    $.ajax({
      url: URL_API_DISTRICTS,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToTable(res);
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIGetAllProvinces() {
    $.ajax({
      url: URL_API_PROVINCES,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToProvinceSelect(res);
        gProvinces = res;
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIGetDistrictById() {
    $.ajax({
      url: URL_API_DISTRICTS + "/" + gId,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUpdateForm(res);
        $("#update-modal").modal("show");
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPICreateDistrict(paramDistrict) {
    $.ajax({
      url: URL_API_DISTRICTS,
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramDistrict),
      dataType: "json",
      success: function (res) {
        console.log(res);
        alert("Tạo mới thành công");
        $("#create-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        alert("Tạo mới thất bại");
      },
    });
  }

  function callAPIUpdateDistrictById(paramDistrict) {
    $.ajax({
      url: URL_API_DISTRICTS + "/" + gId,
      method: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramDistrict),
      dataType: "json",
      success: function (res) {
        console.log(res);
        alert("Cập nhật thành công");
        $("#update-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        alert("Cập nhật thất bại");
      },
    });
  }

  function callAPIDeleteDistrictById() {
    $.ajax({
      url: URL_API_DISTRICTS + "/" + gId,
      method: "DELETE",
      success: function (res) {
        console.log(res);
        alert("Xóa thành công");
        $("#delete-modal").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        alert("Xóa thất bại");
      },
    });
  }

  //handle function
  function loadDataToTable(paramDistrict) {
    gTable.clear();
    gTable.rows.add(paramDistrict);
    gTable.draw();
  }

  function loadDataToUpdateForm(paramResponse) {
    gId = paramResponse.id;
    $("#input-name-update").val(paramResponse.name);
    $("#select-prefix-update").val(paramResponse.prefix);
    $("#select-province-update").val(paramResponse.provinceId);
    let getNgayTao = new Date(convertDateFormat(paramResponse.createdAt));
    $("#input-created-at-update").val(getNgayTao.toLocaleDateString("en-CA")); //en-CA có dạng yyyy-MM-dd
    let getNgayCapNhat = new Date(convertDateFormat(paramResponse.updatedAt));
    $("#input-updated-at-update").val(
      getNgayCapNhat.toLocaleDateString("en-CA")
    );
  }

  function loadDataToProvinceSelect(provinces) {
    $(".province-select").html("");
    for (let i = 0; i < provinces.length; i++) {
      let option = $("<option/>");
      option.prop("value", provinces[i].id);
      option.prop("text", provinces[i].name);
      $(".province-select").append(option);
    }
  }

  // collect function
  function collectIdRowClick(paramIcon) {
    let vRowClick = $(paramIcon).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gId = vRowData.id; //lưu id
  }

  function collectDataCreate(paramDistrict) {
    paramDistrict.name = $("#input-name-create").val();
    paramDistrict.prefix = $("#select-prefix-create").val();
    paramDistrict.province.id = parseInt($("#select-province-create").val());
  }

  function collectDataUpdate(paramDistrict) {
    paramDistrict.name = $("#input-name-update").val();
    paramDistrict.prefix = $("#select-prefix-update").val();
    paramDistrict.province.id = parseInt($("#select-province-update").val());
  }

  function validationData(paramDistrict) {
    if (paramDistrict.name === "") {
      alert("Vui lòng nhập tên");
      return false;
    }
    if (paramDistrict.prefix === "") {
      alert("Vui lòng nhập tiền tố");
      return false;
    }
    if (paramDistrict.province.id === "") {
      alert("Vui lòng chọn tỉnh thành phố");
      return false;
    }
    return true;
  }

  //chuyển ngày dd-MM-yyyy thành yyyy-MM-dd(để trình duyệt hiểu)
  function convertDateFormat(paramDateString) {
    if (paramDateString) {
      const dateParts = paramDateString.split("-");
      return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }
  }

  // Hàm xử lý sự kiện hidden modal create
  function clearFormModalCreate() {
    $("#input-name-create").val("");
    $("#select-prefix-create").val("Huyện");
    $("#select-province-create").val("");
  }

  // Hàm xử lý sự kiện hidden modal update
  function clearFormModalUpdate() {
    $("#input-name-update").val("");
    $("#select-prefix-update").val("Huyện");
    $("#select-province-update").val("");
    $("#input-created-at-update").val("");
    $("#input-updated-at-update").val("");
  }
});
