$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //URL API
  var URL_API_WARDS = "http://localhost:8080/wards";
  var URL_API_PROVINCES = "http://localhost:8080/provinces";
  var URL_API_DISTRICTS_BY_PROVINCE_ID =
    "http://localhost:8080/province/districts?provinceId=";
  var gId;
  var gProvinces;

  // Biến mảng toàn cục chứa danh sách tên các thuộc tính
  const gCOLS = [
    "id",
    "name",
    "prefix",
    "districtName",
    "provinceName",
    "createdAt",
    "updatedAt",
    "action",
  ];

  // Biến toàn cục định nghĩa chỉ số các cột tương ứng
  const gSTT_COL = 0;
  const gNAME_COL = 1;
  const gPREFIX_COL = 2;
  const gDISTRICT_NAME_COL = 3;
  const gPROVINCE_NAME_COL = 4;
  const gCREATE_DAY_COL = 5;
  const gUPDATE_DAY_COL = 6;
  const gACTION_COL = 7;

  //định nghĩa table
  var gTable = $("#ward-table").DataTable({
    columns: [
      { data: gCOLS[gSTT_COL] },
      { data: gCOLS[gNAME_COL] },
      { data: gCOLS[gPREFIX_COL] },
      { data: gCOLS[gDISTRICT_NAME_COL] },
      { data: gCOLS[gPROVINCE_NAME_COL] },
      { data: gCOLS[gCREATE_DAY_COL] },
      { data: gCOLS[gUPDATE_DAY_COL] },
      { data: gCOLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        target: gSTT_COL,
        className: "text-center",
        render: (data, type, row, meta) => {
          return meta.row + 1;
        },
      },
      {
        target: gACTION_COL,
        className: "text-center",
        defaultContent: `
          <div class="d-flex justify-content-around">
            <i class="fas fa-edit update-icon btn text-primary mx-auto"></i>
            <i class="fas fa-trash-alt delete-icon btn text-danger mx-auto"></i>
          </div>
        `,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  //Event show modal
  $("#create-modal, #update-modal").on("shown.bs.modal", function () {
    loadDataToProvinceSelect(gProvinces);
  });

  //sự kiện thay đổi select province(do ko thể tạo và update cùng lúc nên bắt chung sự kiện)
  $(".province-select").on("change", async function () {
    //xóa trắng select district để tránh ng dùng bị nhầm lẫn dữ liệu trong khi chờ callAPI
    $(".district-select").html("").prop("disabled", true);
    //Lấy provinceId tương ứng province đang đc lựa chọn
    let provinceId = $(this).val();
    //call API
    await callAPIGetDistrictsByProvinceId(provinceId);
  });

  //Event click btn thêm mới
  $("#btn-create").click(async () => {
    let provinceId = $(".province-select").val();
    // Kiểm tra nếu giá trị không rỗng thì sử dụng val(), ngược lại sử dụng giá trị đầu tiên trong gProvinces
    if (!provinceId) {
      provinceId = gProvinces.length > 0 ? gProvinces[0].id : null;
    }
    await callAPIGetDistrictsByProvinceId(provinceId);
    $("#create-modal").modal("show");
  });

  //Event click icon update
  $("#ward-table").on("click", ".update-icon", function () {
    onIconUpdateClick(this);
  });

  //Event click icon delete
  $("#ward-table").on("click", ".delete-icon", function () {
    onIconDeleteClick(this);
  });

  //Event click btn confirm create
  $("#btn-confirm-create").click(onBtnConfirmCreateClick);

  // Event click btn confirm update
  $("#btn-confirm-update").click(onBtnConfirmUpdateClick);

  //Event click btn confirm delete
  $("#btn-confirm-delete").click(callAPIDeleteWardById);

  //Event hidden modal create
  $("#create-modal").on("hidden.bs.modal", clearFormModalCreate);

  //Event hidden modal update
  $("#update-modal").on("hidden.bs.modal", clearFormModalUpdate);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  async function onPageLoading() {
    try {
      callAPIGetAllWards();
      await callAPIGetAllProvinces();
      const provinceId = $(".province-select").val();
      await callAPIGetDistrictsByProvinceId(provinceId);
    } catch (error) {
      console.error("Error:", error);
    }
  }

  // Hàm xử lý sự kiện click icon update
  function onIconUpdateClick(paramUpdateIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramUpdateIcon);
    //B3: call API get by ID
    callAPIGetWardById();
  }

  // Hàm xử lý sự kiện click icon delete
  function onIconDeleteClick(paramDeleteIcon) {
    //B1: Thu thập dữ liệu Id
    collectIdRowClick(paramDeleteIcon);
    $("#delete-modal").modal("show");
  }

  // Hàm xử lý sự kiện click nút confirm tạo mới
  function onBtnConfirmCreateClick() {
    // đối tượng sẽ được tạo mới
    var wardObj = {
      name: "",
      prefix: "",
      district: {
        id: "",
      },
    };
    // B1: Thu thập dữ liệu
    collectDataCreate(wardObj);
    // B2: Validate
    let vCheck = validationData(wardObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPICreateWard(wardObj);
    }
  }

  // Hàm xử lý sự kiện click nút confirm update
  function onBtnConfirmUpdateClick() {
    // đối tượng sẽ được tạo mới
    var wardObj = {
      name: "",
      prefix: "",
      district: {
        id: "",
      },
    };
    // B1: Thu thập dữ liệu
    collectDataUpdate(wardObj);
    // B2: Validate
    let vCheck = validationData(wardObj);
    if (vCheck) {
      // B3: Call API và xử lý hiển thị
      callAPIUpdateWardById(wardObj);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Call API
  function callAPIGetAllWards() {
    $.ajax({
      url: URL_API_WARDS,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToTable(res);
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPIGetAllProvinces() {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: URL_API_PROVINCES,
        method: "GET",
        success: function (res) {
          console.log(res);
          loadDataToProvinceSelect(res);
          gProvinces = res;
          resolve(res);
        },
        error: function (err) {
          console.error(err);
          reject(err);
        },
      });
    });
  }

  function callAPIGetDistrictsByProvinceId(provinceId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: URL_API_DISTRICTS_BY_PROVINCE_ID + provinceId,
        method: "GET",
        success: function (res) {
          console.log(res);
          loadDataToDistrictSelect(res);
          resolve(res);
        },
        error: function (err) {
          console.error(err);
          reject(err);
        },
      });
    });
  }

  function callAPIGetWardById() {
    $.ajax({
      url: URL_API_WARDS + "/" + gId,
      method: "GET",
      success: function (res) {
        console.log(res);
        loadDataToUpdateForm(res);
        $("#update-modal").modal("show");
      },
      error: function (err) {
        console.error(err);
      },
    });
  }

  function callAPICreateWard(paramWard) {
    $.ajax({
      url: URL_API_WARDS,
      method: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramWard),
      dataType: "json",
      success: function (res) {
        console.log(res);
        alert("Tạo mới thành công");
        $("#create-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        alert("Tạo mới thất bại");
      },
    });
  }

  function callAPIUpdateWardById(paramWard) {
    $.ajax({
      url: URL_API_WARDS + "/" + gId,
      method: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramWard),
      dataType: "json",
      success: function (res) {
        console.log(res);
        alert("Cập nhật thành công");
        $("#update-modal").modal("hide"); //ẩn modal
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        alert("Cập nhật thất bại");
      },
    });
  }

  function callAPIDeleteWardById() {
    $.ajax({
      url: URL_API_WARDS + "/" + gId,
      method: "DELETE",
      success: function (res) {
        console.log(res);
        alert("Xóa thành công");
        $("#delete-modal").modal("hide");
        onPageLoading();
      },
      error: function (err) {
        console.error(err);
        alert("Xóa thất bại");
      },
    });
  }

  //handle function
  function loadDataToTable(paramDistrict) {
    gTable.clear();
    gTable.rows.add(paramDistrict);
    gTable.draw();
  }

  function loadDataToProvinceSelect(provinces) {
    $(".province-select").html("");
    for (let i = 0; i < provinces.length; i++) {
      let option = $("<option/>");
      option.prop("value", provinces[i].id);
      option.prop("text", provinces[i].name);
      $(".province-select").append(option);
    }
  }

  function loadDataToDistrictSelect(districts) {
    $(".district-select").html("");
    if (districts.length > 0) {
      $(".district-select").prop("disabled", false); //bật select

      for (let i = 0; i < districts.length; i++) {
        let option = $("<option/>");
        option.prop("value", districts[i].id);
        option.prop("text", districts[i].name);
        $(".district-select").append(option);
      }
    }
  }

  async function loadDataToUpdateForm(paramResponse) {
    gId = paramResponse.id;

    $("#input-name-update").val(paramResponse.name);
    $("#select-prefix-update").val(paramResponse.prefix);
    //
    $("#select-province-update").val(paramResponse.provinceId);
    let getNgayTao = new Date(convertDateFormat(paramResponse.createdAt));
    $("#input-created-at-update").val(getNgayTao.toLocaleDateString("en-CA")); //en-CA có dạng yyyy-MM-dd
    let getNgayCapNhat = new Date(convertDateFormat(paramResponse.updatedAt));
    $("#input-updated-at-update").val(
      getNgayCapNhat.toLocaleDateString("en-CA")
    );

    //chờ load hết dữ liệu vào district-select đã rồi mới gán
    //do async nên đặt cuối cùng để tối ưu hiệu suất
    await callAPIGetDistrictsByProvinceId(paramResponse.provinceId);
    $("#select-district-update").val(paramResponse.districtId);
  }

  // collect function
  function collectIdRowClick(paramIcon) {
    let vRowClick = $(paramIcon).closest("tr");
    let vRowData = gTable.row(vRowClick).data();
    gId = vRowData.id; //lưu id
  }

  function collectDataCreate(paramWard) {
    paramWard.name = $("#input-name-create").val();
    paramWard.prefix = $("#select-prefix-create").val();
    console.log($("#select-prefix-create").val());
    paramWard.district.id = parseInt($("#select-district-create").val());
  }

  function collectDataUpdate(paramWard) {
    paramWard.name = $("#input-name-update").val();
    paramWard.prefix = $("#select-prefix-update").val();
    paramWard.district.id = parseInt($("#select-district-update").val());
  }

  function validationData(paramWard) {
    if (paramWard.name === "") {
      alert("Vui lòng nhập tên");
      return false;
    }
    if (paramWard.prefix === "") {
      alert("Vui lòng nhập tiền tố");
      return false;
    }
    if (paramWard.district.id === "") {
      alert("Vui lòng chọn quận huyện");
      return false;
    }
    return true;
  }

  //chuyển ngày dd-MM-yyyy thành yyyy-MM-dd(để trình duyệt hiểu)
  function convertDateFormat(paramDateString) {
    if (paramDateString) {
      const dateParts = paramDateString.split("-");
      return dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
    }
  }

  // Hàm xử lý sự kiện hidden modal create
  function clearFormModalCreate() {
    $("#input-name-create").val("");
    $("#select-prefix-create").val("Xã");
    $("#select-district-create").val("");
    $("#select-province-create").val("");
  }

  // Hàm xử lý sự kiện hidden modal update
  function clearFormModalUpdate() {
    $("#input-name-update").val("");
    $("#select-prefix-update").val("Xã");
    $("#select-district-update").val("");
    $("#select-province-update").val("");
    $("#input-created-at-update").val("");
    $("#input-updated-at-update").val("");
  }
});
