package com.devcamp.task66_devcampprovince.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task66_devcampprovince.entities.District;
import com.devcamp.task66_devcampprovince.entities.Province;
import com.devcamp.task66_devcampprovince.errors.ResourceNotFoundException;
import com.devcamp.task66_devcampprovince.services.ProvinceService;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProvinceController {
    @Autowired
    private ProvinceService provinceService;

    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvinces() {
        try {
            List<Province> provinces = provinceService.getAllProvinces();
            if (provinces != null && !provinces.isEmpty()) {
                return new ResponseEntity<>(provinces, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province/districts")
    public ResponseEntity<Set<District>> getDistrictsByProvinceId(
            @RequestParam(value = "provinceId", required = true) Integer id) {
        try {
            Set<District> districts = provinceService.getDistrictsByProvinceId(id);
            if (districts != null) {
                return new ResponseEntity<Set<District>>(districts, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/pagination")
    public ResponseEntity<List<Province>> getProvincesPanigation(
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            List<Province> provinces = provinceService.getProvincesPanigation(page, size);
            if (provinces != null) {
                return new ResponseEntity<List<Province>>(provinces, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{provinceId}")
    public ResponseEntity<Object> getProvinceById(@PathVariable(name = "provinceId", required = true) Integer id) {
        try {
            Province province = provinceService.getProvinceById(id);
            return new ResponseEntity<Object>(province, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/provinces")
    public ResponseEntity<Object> createProvince(@Valid @RequestBody Province pProvince) {
        try {
            Province province = provinceService.createProvince(pProvince);
            return new ResponseEntity<Object>(province, HttpStatus.CREATED);
        } catch (DuplicateKeyException e) {
            System.err.println(e.getMessage());
            // return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
            return ResponseEntity.unprocessableEntity().body(e.getMessage());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/provinces/{provinceId}")
    public ResponseEntity<Object> updateProvince(@PathVariable(name = "provinceId") Integer id,
            @Valid @RequestBody Province pProvince) {
        try {
            Province province = provinceService.updateProvine(id, pProvince);
            return new ResponseEntity<Object>(province, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/provinces/{provinceId}")
    public ResponseEntity<Object> deleteProvince(@PathVariable(name = "provinceId") Integer id) {
        try {
            provinceService.deleteProvince(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}