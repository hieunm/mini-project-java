package com.devcamp.task66_devcampprovince.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task66_devcampprovince.entities.District;
import com.devcamp.task66_devcampprovince.entities.Ward;
import com.devcamp.task66_devcampprovince.errors.ResourceNotFoundException;
import com.devcamp.task66_devcampprovince.services.DistrictService;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class DistrictController {
    @Autowired
    private DistrictService districtService;

    @GetMapping("/districts")
    public ResponseEntity<List<District>> getAllDistricts() {
        try {
            List<District> districts = districtService.getAllDistricts();
            if (districts != null && !districts.isEmpty()) {
                return new ResponseEntity<>(districts, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district/wards")
    public ResponseEntity<Set<Ward>> getWardsByDistrictId(
            @RequestParam(value = "districtId", required = true) Integer id) {
        try {
            Set<Ward> wards = districtService.getWardsByDistrictId(id);
            if (wards != null) {
                return new ResponseEntity<Set<Ward>>(wards, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{districtId}")
    public ResponseEntity<Object> getDistrictById(@PathVariable(name = "districtId") Integer id) {
        try {
            District district = districtService.getDistrictById(id);
            return new ResponseEntity<Object>(district, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/districts")
    public ResponseEntity<Object> createDistrict(@Valid @RequestBody District pDistrict) {
        try {
            District district = districtService.createDistrict(pDistrict);
            return new ResponseEntity<Object>(district, HttpStatus.CREATED);
        } catch (DuplicateKeyException e) {
            System.err.println(e.getMessage());
            // return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
            return ResponseEntity.unprocessableEntity().body(e.getMessage());
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/districts/{districtId}")
    public ResponseEntity<Object> updateDistrict(@PathVariable(name = "districtId") Integer id,
            @Valid @RequestBody District pDistrict) {
        try {
            District district = districtService.updateDistrict(id, pDistrict);
            return new ResponseEntity<Object>(district, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/districts/{districtId}")
    public ResponseEntity<Object> deleteDistrict(@PathVariable(name = "districtId") Integer id) {
        try {
            districtService.deleteDistrict(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
