package com.devcamp.task66_devcampprovince.errors;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @SuppressWarnings("null")
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        // Tạo một map để chứa thông tin lỗi và các thuộc tính khác
		Map<String, Object> body = new LinkedHashMap<>();
		// Thêm timestamp vào map
		body.put("timestamp", new Date());
		// Thêm trạng thái vào map
		//body.put("status", status.value());
		// Lấy danh sách các lỗi
		List<String> errors = ex.getBindingResult().getFieldErrors().stream()
				// Sử dụng stream để lấy ra thông điệp lỗi của từng trường
				.map(x -> x.getDefaultMessage()).collect(Collectors.toList());
		// Thêm danh sách lỗi vào map
		body.put("errors", errors);

		// Trả về một ResponseEntity chứa map body, headers và status
		return new ResponseEntity<>(body, headers, status);

    }
    
}
