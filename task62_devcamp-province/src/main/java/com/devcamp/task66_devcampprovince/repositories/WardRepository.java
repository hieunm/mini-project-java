package com.devcamp.task66_devcampprovince.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task66_devcampprovince.entities.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {

}
