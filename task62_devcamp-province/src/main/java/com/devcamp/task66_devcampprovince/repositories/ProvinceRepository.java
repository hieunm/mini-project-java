package com.devcamp.task66_devcampprovince.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task66_devcampprovince.entities.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {
    
}
