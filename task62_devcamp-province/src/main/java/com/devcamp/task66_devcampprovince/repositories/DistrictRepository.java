package com.devcamp.task66_devcampprovince.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task66_devcampprovince.entities.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    List<District> findAllByOrderByProvinceAscPrefixAscIdAscNameAsc();
}
