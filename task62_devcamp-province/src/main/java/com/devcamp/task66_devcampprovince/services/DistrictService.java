package com.devcamp.task66_devcampprovince.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.devcamp.task66_devcampprovince.entities.District;
import com.devcamp.task66_devcampprovince.entities.Province;
import com.devcamp.task66_devcampprovince.entities.Ward;
import com.devcamp.task66_devcampprovince.errors.ResourceNotFoundException;
import com.devcamp.task66_devcampprovince.repositories.DistrictRepository;
import com.devcamp.task66_devcampprovince.repositories.ProvinceRepository;

@Service
public class DistrictService {
    @Autowired
    private DistrictRepository districtRepository;
    @Autowired
    private ProvinceRepository provinceRepository;

    public List<District> getAllDistricts() {
        return districtRepository.findAllByOrderByProvinceAscPrefixAscIdAscNameAsc();
    }

    public Set<Ward> getWardsByDistrictId(Integer id) {
        Optional<District> optinalDistrict = districtRepository.findById(id);
        if (optinalDistrict.isPresent()) {
            District district = optinalDistrict.get();
            return district.getWards();
        } else {
            return null;
        }
    }

    public District getDistrictById(Integer id) {
        Optional<District> optionalDistrict = districtRepository.findById(id);

        if (optionalDistrict.isPresent()) {
            return optionalDistrict.get();
        } else {
            throw new ResourceNotFoundException("Không tìm thấy quận/ huyện");
        }
    }

    public District createDistrict(District pDistrict) {
        Optional<District> optinalDistrict = districtRepository.findById(pDistrict.getId());
        if (optinalDistrict.isPresent()) {
            throw new DuplicateKeyException("Quận/ Huyện đã tồn tại");
        } else {
            Optional<Province> optionalProvince = provinceRepository.findById(pDistrict.getProvince().getId());
            if (optionalProvince.isPresent()) {
                pDistrict.setProvince(optionalProvince.get());
                pDistrict.setCreatedAt(new Date());
                pDistrict.setUpdatedAt(null);
                return districtRepository.save(pDistrict);
            } else {
                throw new ResourceNotFoundException("Không tìm thấy tỉnh/ thành phố tương ứng");
            }
        }
    }

    public District updateDistrict(Integer id, District pDistrict) {
        //check huyện có tồn tại ko
        Optional<District> optionalDistrict = districtRepository.findById(id);

        if (optionalDistrict.isPresent()) {
            //check tỉnh có tồn tại ko
            Optional<Province> optionalProvince = provinceRepository.findById(pDistrict.getProvince().getId());
            if (optionalProvince.isPresent()) {
                District district = optionalDistrict.get();
                district.setName(pDistrict.getName());
                district.setPrefix(pDistrict.getPrefix());
                district.setProvince(optionalProvince.get());
                district.setUpdatedAt(new Date());
                return districtRepository.save(district);
            } else {
                throw new ResourceNotFoundException("Không tìm thấy tỉnh/ thành phố tương ứng");
            }
        } else {
            throw new ResourceNotFoundException("Không tìm thấy quận/ huyện");
        }
    }

    public void deleteDistrict(Integer id) {
        //check huyện có tồn tại ko
        Optional<District> optionalDistrict = districtRepository.findById(id);
        if (optionalDistrict.isPresent()) {
            districtRepository.deleteById(id);
        } else {
            throw new ResourceNotFoundException("Không tìm thấy quận/ huyện");
        }
    }
}
