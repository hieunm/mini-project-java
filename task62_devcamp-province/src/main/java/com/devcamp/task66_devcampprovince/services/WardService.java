package com.devcamp.task66_devcampprovince.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.devcamp.task66_devcampprovince.entities.District;
import com.devcamp.task66_devcampprovince.entities.Ward;
import com.devcamp.task66_devcampprovince.errors.ResourceNotFoundException;
import com.devcamp.task66_devcampprovince.repositories.DistrictRepository;
import com.devcamp.task66_devcampprovince.repositories.WardRepository;

@Service
public class WardService {
    @Autowired
    private WardRepository wardRepository;
    @Autowired
    private DistrictRepository districtRepository;

    public List<Ward> getAllWards() {
        return wardRepository.findAll();
    }

    public Ward getWardById(Integer id) {
        Optional<Ward> optionalWard = wardRepository.findById(id);

        if (optionalWard.isPresent()) {
            return optionalWard.get();
        } else {
            throw new ResourceNotFoundException("Không tìm thấy xã/ phường");
        }
    }

    public Ward createWard(Ward pWard) {
        // check xã phường tồn tại
        Optional<Ward> optionalWard = wardRepository.findById(pWard.getId());

        if (optionalWard.isPresent()) {
            throw new DuplicateKeyException("Xã/ Phường đã tồn tại");
        } else {
            // check huyện
            Optional<District> optionalDistrict = districtRepository.findById(pWard.getDistrict().getId());
            if (optionalDistrict.isPresent()) {
                pWard.setDistrict(optionalDistrict.get());
                pWard.setCreatedAt(new Date());
                pWard.setUpdatedAt(null);
                return wardRepository.save(pWard);
            } else {
                throw new ResourceNotFoundException("Không tìm thấy quận/ huyện tương ứng");
            }
        }
    }

    // Cách ngắn hơn dùng orElseThrow() của Optional
    // public Ward createWard(Ward pWard) {
    //     // check trùng lặp xã
    //     wardRepository.findById(pWard.getId())
    //             .ifPresent(existingWard -> {throw new DuplicateKeyException("Xã/ Phường đã tồn tại");});
    //     // check huyện not found
    //     District district = districtRepository.findById(pWard.getDistrict().getId())
    //             .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy quận/ huyện tương ứng"));

    //     pWard.setCreatedAt(new Date());
    //     pWard.setUpdatedAt(null);
    //     return wardRepository.save(pWard);
    // }

    public Ward updateWard(Integer id, Ward pWard) {
        // check xã có tồn tại ko
        Optional<Ward> optionalWard = wardRepository.findById(id);

        if (optionalWard.isPresent()) {
            // check huyện có tồn tại ko
            Optional<District> optionalDistrict = districtRepository.findById(pWard.getDistrict().getId());
            if (optionalDistrict.isPresent()) {
                Ward ward = optionalWard.get();
                ward.setName(pWard.getName());
                ward.setPrefix(pWard.getPrefix());
                ward.setDistrict(optionalDistrict.get());
                ward.setUpdatedAt(new Date());
                return wardRepository.save(ward);
            } else {
                throw new ResourceNotFoundException("Không tìm thấy quận/ huyện tương ứng");
            }
        } else {
            throw new ResourceNotFoundException("Không tìm thấy xã/ phường");
        }
    }
    // Cách ngắn hơn dùng orElseThrow() của Optional
    // public Ward updateWard(Integer id, Ward pWard) {
    // // Kiểm tra xã có tồn tại không
    // Ward ward = wardRepository.findById(id)
    // .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy xã/
    // phường"));

    // // Kiểm tra quận/huyện có tồn tại không
    // districtRepository.findById(pWard.getDistrict().getId())
    // .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy quận/ huyện
    // tương ứng"));

    // // Cập nhật thông tin và lưu vào cơ sở dữ liệu
    // ward.setUpdatedAt(new Date());
    // return wardRepository.save(ward);
    // }

    public void deleteWard(Integer id) {
        wardRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy xã/ phường"));
        wardRepository.deleteById(id);
    }
}
