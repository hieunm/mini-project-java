package com.devcamp.task66_devcampprovince.entities;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "district")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Nhập tên quận/huyện")
    @Size(min = 1, max = 50, message = "Tên quận/huyện phải có độ dài từ 1 đến 50 ký tự")
    @Column(name = "name")
    private String name;

    @NotEmpty(message = "Nhập tiền tố quận/huyện")
    @Size(min = 1, max = 20, message = "Tiền tố quận/huyện phải có độ dài từ 1 đến 20 ký tự")
    @Column(name = "prefix")
    private String prefix;

    @NotNull(message = "Nhập tỉnh/ thành phố tương ứng")
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "province_id")
    private Province province;

    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Ward> wards;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date updatedAt;

    @Transient
    private int provinceId;

    @Transient
    private String provinceName;

    public int getProvinceId() {
        return this.province.getId();
    }

    public void setProvinceId(int provinceId) {
        this.province.setId(provinceId);
    }

    public String getProvinceName() {
        return this.province.getName();
    }

    public void setProvinceName(String provinceName) {
        this.province.setName(provinceName);
    }

    public District() {
    }

    public District(int id,
            @NotEmpty(message = "Nhập tên quận/huyện") @Size(min = 2, max = 50, message = "Tên quận/huyện phải có độ dài từ 2 đến 50 ký tự") String name,
            @NotEmpty(message = "Nhập tiền tố quận/huyện") @Size(min = 1, max = 20, message = "Tiền tố quận/huyện phải có độ dài từ 1 đến 20 ký tự") String prefix,
            Province province, Set<Ward> wards, Date createdAt, Date updatedAt) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.wards = wards;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public Set<Ward> getWards() {
        return wards;
    }

    public void setWards(Set<Ward> wards) {
        this.wards = wards;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
