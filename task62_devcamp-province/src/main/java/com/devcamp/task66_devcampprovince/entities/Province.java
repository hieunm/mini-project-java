package com.devcamp.task66_devcampprovince.entities;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "province")
public class Province {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Nhập mã tỉnh/thành phố")
    @Size(min = 1, max = 10, message = "Mã tỉnh/thành phố phải có độ dài từ 1 đến 10 ký tự")
    @Column(name = "code")
    private String code;

    @NotEmpty(message = "Nhập tên tỉnh/thành phố")
    @Size(min = 1, max = 50, message = "Tên tỉnh/thành phố phải có độ dài từ 1 đến 50 ký tự")
    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "province", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<District> districts;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date updatedAt;

    public Province() {
    }

    public Province(int id,
            @NotEmpty(message = "Nhập mã tỉnh/thành phố") @Size(min = 1, max = 10, message = "Mã tỉnh/thành phố phải có độ dài từ 1 đến 10 ký tự") String code,
            @NotEmpty(message = "Nhập tên tỉnh/thành phố") @Size(min = 2, max = 50, message = "Tên tỉnh/thành phố phải có độ dài từ 2 đến 50 ký tự") String name,
            Set<District> districts, Date createdAt, Date updatedAt) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.districts = districts;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
