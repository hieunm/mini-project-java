package com.devcamp.task66_devcampprovince.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "ward")
public class Ward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Nhập tên xã/phường")
    @Size(min = 1, max = 50, message = "Tên xã/phường phải có độ dài từ 1 đến 50 ký tự")
    @Column(name =  "name")
    private String name;

    @NotEmpty(message = "Nhập tiền tố xã/phường")
    @Size(min = 1, max = 20, message = "Tiền tố xã/phường phải có độ dài từ 1 đến 20 ký tự")
    @Column(name = "prefix")
    private String prefix;

    @NotNull(message = "Nhập quận/ huyện tương ứng")
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "district_id")
    private District district;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date updatedAt;

    @Transient
    private int districtId;

    @Transient
    private String districtName;

    @Transient
    private int provinceId;

    @Transient
    private String provinceName;

    public int getDistrictId() {
        return this.district.getId();
    }

    public void setDistrictId(int districtId) {
        this.district.setId(districtId);
    }

    public String getDistrictName() {
        return this.district.getName();
    }

    public void setDistrictName(String districtName) {
        this.district.setName(districtName);
    }

    public int getProvinceId() {
        return this.district.getProvinceId();
    }

    public void setProvinceId(int provinceId) {
        this.district.setProvinceId(provinceId);;
    }

    public String getProvinceName() {
        return this.district.getProvinceName();
    }

    public void setProvinceName(String provinceName) {
        this.district.setProvinceName(provinceName);
    }

    public Ward() {
    }

    public Ward(int id,
            @NotEmpty(message = "Nhập tên xã/phường") @Size(min = 2, max = 50, message = "Tên xã/phường phải có độ dài từ 2 đến 50 ký tự") String name,
            @NotEmpty(message = "Nhập tiền tố xã/phường") @Size(min = 1, max = 20, message = "Tiền tố xã/phường phải có độ dài từ 1 đến 20 ký tự") String prefix,
            District district, Date createdAt, Date updatedAt) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.district = district;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    

}
