package com.devcamp.task66_devcampprovince;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task62SpringbootApiWithMysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task62SpringbootApiWithMysqlApplication.class, args);
	}

}
